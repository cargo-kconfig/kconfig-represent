/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

mod ast_extra;
mod config_registry;
pub mod dependencies;
pub mod error;
mod eval;
mod hex;
mod item;
#[cfg(test)]
mod tests;
mod variant;

pub use hex::Hex;

pub(crate) use ast_extra::AstExtra;

pub use variant::Variant;
pub use variant::VariantDataType;

use std::{cmp::Ordering, fmt::Display, ops::Not};

pub use config_registry::ConfigRegistry;
pub use error::{Error, LoadError};
pub use item::Item;
use kconfig_parser::{Ast, Symbol};

/// A tristate type is basically a bool wrapped in an option, where
/// None represents 'm', or the 'maybe' state.
#[derive(Clone, Debug, PartialEq, Eq, Copy)]
pub struct Tristate(Option<bool>);

impl Default for Tristate {
    fn default() -> Self {
        Tristate::FALSE
    }
}

impl PartialOrd for Tristate {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.int_comp(other))
    }
}

impl Ord for Tristate {
    fn cmp(&self, other: &Self) -> Ordering {
        self.int_comp(other)
    }
}

impl Not for Tristate {
    type Output = Tristate;

    fn not(self) -> Self::Output {
        match self {
            Tristate::TRUE => Tristate::FALSE,
            Tristate::FALSE => Tristate::TRUE,
            Tristate::MAYBE => Tristate::MAYBE,
        }
    }
}

impl Display for Tristate {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Tristate::TRUE => f.write_str(&format!("{}", true)),
            Tristate::FALSE => f.write_str(&format!("{}", false)),
            Tristate::MAYBE => f.write_str("maybe"),
        }
    }
}

impl From<&str> for Tristate {
    fn from(v: &str) -> Self {
        match v {
            "n" | "N" => Tristate::FALSE,
            "y" | "Y" => Tristate::TRUE,
            _ => Tristate::MAYBE,
        }
    }
}

impl From<bool> for Tristate {
    fn from(b: bool) -> Self {
        match b {
            true => Tristate::TRUE,
            false => Tristate::FALSE,
        }
    }
}

impl Tristate {
    pub const MAYBE: Self = Self(None);
    pub const TRUE: Self = Self(Some(true));
    pub const FALSE: Self = Self(Some(false));

    fn int_comp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (&Self::TRUE, &Self::TRUE)
            | (&Self::MAYBE, &Self::MAYBE)
            | (&Self::FALSE, &Self::FALSE) => std::cmp::Ordering::Equal,
            (&Self::TRUE, &Self::MAYBE)
            | (&Self::TRUE, &Self::FALSE)
            | (&Self::MAYBE, &Self::FALSE) => std::cmp::Ordering::Greater,
            _ => std::cmp::Ordering::Less,
        }
    }
}

/// Determines the type, and name of a given menu item. This can either be
/// - A Configuration Item (Config)
/// - A Regular Menu (Menu)
/// - A Menu Configuration Item, which is a hybrid of the former two (MenuConfig)
/// If a Menu is defined, the name of the menu will be the full name, if any
/// of the other two are defined, it will be the configuration name which is
/// typically also used in the .config file.
#[derive(Hash, PartialEq, Eq, Clone, Debug, PartialOrd, Ord)]
pub enum ItemReference {
    Config(String),
    Menu(String),
    MenuConfig(String),
}

impl Default for ItemReference {
    fn default() -> Self {
        ItemReference::Config("".to_owned())
    }
}

/// Converts a menu item descriptor directly into the representation
/// of the name of the menu item.
impl Display for ItemReference {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            ItemReference::Config(s) | ItemReference::Menu(s) | ItemReference::MenuConfig(s) => s,
        })
    }
}

/// Descriptor information contains the "help" text of a menu item.
/// (where applicable). It also contains a item reference, to be able to lookup
/// the originating item.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Info {
    text: Vec<String>,
    origin: ItemReference,
}

impl Info {
    /// Creates the descriptor information for the given item reference
    pub(crate) fn new(text: Vec<String>, origin: ItemReference) -> Self {
        Self { text, origin }
    }

    /// Returns wether the given help text is empty, and perhaps not relevant
    pub fn is_empty(&self) -> bool {
        self.text.is_empty()
    }

    /// Returns the origin item reference belonging to the information
    pub fn origin(&self) -> ItemReference {
        self.origin.clone()
    }

    /// Returns an iterator over the text
    pub fn iter(&self) -> Box<dyn Iterator<Item = String>> {
        Box::new(self.text.clone().into_iter())
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum MutationStart {
    String(String),
    Hex(Hex),
    Int(i64),
    Bool(bool),
    Tristate(Tristate),
    None,
}

impl From<Variant> for MutationStart {
    fn from(value: Variant) -> Self {
        match value.datatype() {
            VariantDataType::Bool => MutationStart::Bool(value.into()),
            VariantDataType::Hex => MutationStart::Hex(value.into()),
            VariantDataType::Int => MutationStart::Int(value.into()),
            VariantDataType::String => MutationStart::String(value.into()),
            VariantDataType::Tristate => MutationStart::Tristate(value.into()),
        }
    }
}
