/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//! This file implements the registry, which is the main component of the Kconfig
//! representation module.

use crate::dependencies::ResolvedExpr;
use crate::error::LoadError;
use crate::item::ConfigItem;
use crate::Tristate;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Lines;
use std::io::Read;

use crate::{
    dependencies::{DependencyResolver, SelectAndImplyDependencyResolver},
    error::Error,
    eval::Evaluator,
    eval::ExpensiveEvaluator,
    item::MaybeIncompleteItemReference,
    Ast, AstExtra, Info, Item, ItemReference, MutationStart, Symbol, Variant,
};
use std::collections::{HashMap, HashSet};
use std::fs::OpenOptions;
use std::io::Write;

/// The ConfigRegistry is used to create a registry, which allows reading and writing
/// a configuration to and from a .config file as defined by a Kconfig file. To
/// present the menu, a frontend is necessary to represent the state of the registry.
#[derive(Debug)]
pub struct ConfigRegistry {
    // An item can either be a configuration item, a menu item, or both (in the case
    // of a "menuconfig" situation). The different items can be retrieved by a
    // menu item descriptor, and resolved as such.
    items: HashMap<ItemReference, Item>,

    // Name of the main menu
    main_menu_name: String,
}

impl ConfigRegistry {
    /// Constructs a new configuration registry, given the Abstract syntax tree
    /// as loaded from a kconfig-parser's Ast instance, and the macro symbols
    /// discoverd by the macro lexer (if used).
    pub fn new(ast: &Ast, macro_symbols: &HashSet<Symbol>) -> Result<Self, Error> {
        let mut forward_dependency_resolver = SelectAndImplyDependencyResolver::new();
        let mut main_menu = Self {
            items: HashMap::new(),
            main_menu_name: ast.main_menu_name_with_default(),
        };

        let items = Item::new_from_ast(
            &main_menu.main_menu_name,
            macro_symbols,
            &ast,
            &mut forward_dependency_resolver,
        );

        main_menu.items.extend(items.into_iter());

        DependencyResolver::new(forward_dependency_resolver, macro_symbols.clone())
            .resolve(&mut main_menu.items);

        main_menu.reset()?;
        Ok(main_menu)
    }

    /// Return a reference to the map of the items available in the configuration registry
    pub fn items<'a>(&'a self) -> &'a HashMap<ItemReference, Item> {
        &self.items
    }

    /// Returns the name of the main menu
    pub fn main_menu_name<'a>(&'a self) -> &'a str {
        &self.main_menu_name
    }

    /// Returns the item belonging to the main menu
    pub fn main_menu_item<'a>(&'a self) -> &'a Item {
        let itemref = ItemReference::Menu(self.main_menu_name.clone());
        // The menu should be in the item map, otherwise something very strange is going on
        self.get(&itemref).unwrap()
    }

    /// Finds the item given the item reference, if available, otherwise returns None
    pub fn get<'a>(&'a self, itemref: &ItemReference) -> Option<&'a Item> {
        self.items.get(itemref)
    }

    /// Returns an item's information, if available, otherwise returns None
    pub fn get_info(&self, itemref: &ItemReference) -> Option<Info> {
        self.get(itemref).map(|item| item.info())
    }

    /// Given the item reference, determine whether the associated item is enabled
    pub fn item_enabled(&self, itemref: &ItemReference) -> Result<bool, Error> {
        match self.items.get(itemref) {
            Some(item) => {
                let evaluator = Evaluator::new(&self.items);
                let enabled_variant = evaluator.enabled(item);
                enabled_variant.map(Variant::into)
            }
            None => Err(Error::from(format!(
                "Could not find item associated to itemreference {itemref}"
            ))),
        }
    }

    fn cloned_int_current_config_item(
        item: Item,
        expensive_evaluator: &ExpensiveEvaluator,
    ) -> Option<ConfigItem> {
        let mut restricted_config_item: Option<&ConfigItem> = None;
        let mut unrestricted_config_item: Option<&ConfigItem> = None;

        for (_, config_item) in item.config_items() {
            if let Some(expr) = config_item.maybe_dependencies() {
                if let Ok(variant) = expensive_evaluator.evalexpr(expr.item_expr()) {
                    let enabled: bool = variant.into();
                    if enabled {
                        restricted_config_item = Some(config_item);
                    }
                }
            } else {
                unrestricted_config_item = Some(config_item);
            }
        }

        match (restricted_config_item, unrestricted_config_item) {
            (Some(config_item), _) => Some(config_item.clone()),
            (_, Some(config_item)) => Some(config_item.clone()),
            _ => None,
        }
    }

    fn int_current_config_item<'a>(&self, item: &'a Item) -> Option<&'a ConfigItem> {
        let mut restricted_config_item: Option<&ConfigItem> = None;
        let mut unrestricted_config_item: Option<&ConfigItem> = None;

        for (_, config_item) in item.config_items() {
            if let Some(expr) = config_item.maybe_dependencies() {
                let evaluator = Evaluator::new(&self.items);
                if let Ok(variant) = evaluator.evalexpr(expr.item_expr()) {
                    let enabled: bool = variant.into();
                    if enabled {
                        restricted_config_item = Some(config_item);
                    }
                }
            } else {
                unrestricted_config_item = Some(config_item);
            }
        }

        match (restricted_config_item, unrestricted_config_item) {
            (Some(config_item), _) => Some(&config_item),
            (_, Some(config_item)) => Some(&config_item),
            _ => None,
        }
    }

    /// Returns a clone of the active configuration item, if available
    pub fn current_config_item(&self, itemref: &ItemReference) -> Option<ConfigItem> {
        self.get(itemref)
            .map(|item| self.int_current_config_item(item))
            .map(|c| c.map(ConfigItem::clone))
            .flatten()
    }

    /// Returns how to start a mutation. Given the configuration type, this can be
    /// a different start situation.
    pub fn mutation_start(&self, itemref: &ItemReference) -> Option<MutationStart> {
        self.current_value(itemref).map(MutationStart::from)
    }

    /// The current value of an item is resolved according to a number of rules:
    ///
    /// If the value has been selected through a select reverse dependency, then
    /// this value will be used, with respect to it's default value.
    ///
    /// If the value has a "regular" value, then this value will be used
    ///
    /// If the value has been selected through an implied value, then this
    /// value will be used, with respect to it's default value.
    ///
    /// Otherwise, the default value will be used.
    pub fn current_value(&self, itemref: &ItemReference) -> Option<Variant> {
        Evaluator::new(&self.items).current_value(itemref)
    }

    /// Determines whether the value if the configuration item internal to the item
    /// is set to it's default value.
    pub fn current_value_is_default(&self, itemref: &ItemReference) -> bool {
        let maybe_config_item = self.current_config_item(itemref);
        maybe_config_item
            .map(|config_item| match config_item.value() {
                Some(_) => false,
                None => true,
            })
            .unwrap_or(false)
    }

    fn int_set_value(&mut self, itemref: &ItemReference, value: Variant) -> Result<(), Error> {
        // If the itemreference is of type menu, this is a nonsense situation
        if let &ItemReference::Menu(_) = itemref {
            return Err(Error::from(format!(
                "Item reference {itemref} indicates a menu, which does not have values"
            )));
        }

        // Get the item, if available
        let item = match self.items.get_mut(itemref) {
            // If no item is available, then an error should be raised
            None => {
                return Err(Error::from(format!(
                    "Item for reference not found: {itemref}"
                )))
            }
            Some(item) => item,
        };

        // Force the value of each configuration item within the current item
        for (_, config_item) in item.config_items_mut() {
            config_item.set_config_value(value.clone());
        }

        Ok(())
    }

    fn int_discard_value(&mut self, itemref: &ItemReference) -> Result<(), Error> {
        // If the itemreference is of type menu, this is a nonsense situation
        if let &ItemReference::Menu(_) = itemref {
            return Err(Error::from(format!(
                "Item reference {itemref} indicates a menu, which does not have values"
            )));
        }

        // Get the item, if available
        let item = match self.items.get_mut(itemref) {
            // If no item is available, then an error should be raised
            None => {
                return Err(Error::from(format!(
                    "Item for reference not found: {itemref}"
                )))
            }
            Some(item) => item,
        };

        // Force the value of each configuration item within the current item
        for (_, config_item) in item.config_items_mut() {
            config_item.reset_value();
        }

        Ok(())
    }

    /// Sets the value of a configuration item either by loading it from an
    /// earlier stored session, or by entering the data programmatically/through
    /// a configuration editor. Returns nothing if successful, or an error if
    /// for some reason the value could not be set properly.
    pub fn set_value(&mut self, itemref: &ItemReference, value: Variant) -> Result<(), Error> {
        self.int_set_value(itemref, value)?;
        self.reset()
    }

    /// Unsets the value of a configuation item
    pub fn discard_value(&mut self, itemref: &ItemReference) -> Result<(), Error> {
        self.int_discard_value(itemref)?;
        self.reset()
    }

    /// Finds the appropriate item reference by the name of the itemreference
    pub fn find_itemref(&self, name: &str) -> Option<ItemReference> {
        let key1 = ItemReference::Config(name.to_string());
        let key2 = ItemReference::MenuConfig(name.to_string());
        let key3 = ItemReference::Menu(name.to_string());
        if self.items.contains_key(&key1) {
            Some(key1)
        } else if self.items.contains_key(&key2) {
            Some(key2)
        } else if self.items.contains_key(&key3) {
            Some(key3)
        } else {
            None
        }
    }

    /// Evaluates all items and enforces the proper rules on all internal configuration
    /// items and dependent configuration items for all the items evaluated.
    pub fn reset(&mut self) -> Result<(), Error> {
        // Items are reset by evaluating them, from the top-most item, to the item with the
        // most dependencies.

        // First, collect all the item references, and the dependencies of those item references
        let mut snapshots: HashMap<ItemReference, HashSet<ItemReference>> = HashMap::new();
        for (itemref, item) in &self.items {
            let maybe_incomplete_snapshot = item.collect_all_dependencies();
            let snapshot = maybe_incomplete_snapshot
                .iter()
                .map(|iiref| match iiref {
                    MaybeIncompleteItemReference::ItemReference(itemref) => itemref.clone(),
                    MaybeIncompleteItemReference::String(s) => self
                        .find_itemref(&s)
                        .unwrap_or(ItemReference::Config(s.to_string())),
                })
                .collect();
            snapshots.insert(itemref.clone(), snapshot);
        }

        // Resolve the item references which have no further dependencies, then resolve the
        // next items, etc. until the list of snapshots is depleted. If the list of snapshots
        // does not deplete any more, then there is a cyclic dependency, which is to result
        // into an error.
        while snapshots.len() > 0 {
            match Self::take_available_snapshot(&snapshots) {
                Some(itemref) => {
                    self.reset_item_by_ref(itemref.clone());
                    snapshots.remove(&itemref);
                }
                None => {
                    if snapshots.len() > 0 {
                        let keys: HashSet<ItemReference> = snapshots.keys().cloned().collect();
                        let mut error_string = String::new();
                        for key in keys {
                            if error_string.len() > 0 {
                                error_string += " ";
                            }
                            error_string += &key.to_string();
                        }
                        return Err(Error::from(format!(
                            "Dependency cycle(s) detected in items {error_string}"
                        )));
                    }
                }
            }
        }

        Ok(())
    }

    fn reset_item_by_ref(&mut self, itemref: ItemReference) {
        let map: HashMap<ItemReference, Item> = self
            .items
            .iter()
            .map(|(itemref, item)| (itemref.clone(), item.clone()))
            .collect();

        if let Some(item) = self.items.get_mut(&itemref) {
            // For the item:
            // Determine if the item is enabled, an item is only enabled if all the
            // item dependencies evaluate true.
            //
            // Determine which current configuration item is active, all other configuration
            // will be defaulted.
            // - Determine if the value of the current configuration item is
            //   selected and/or implied, and resolve the choices
            // - Determine if the value has a default value matching the configured value,
            //   if so, erase the configured value
            // - If no default value matches the configured value, set the configured value

            let evaluator = ExpensiveEvaluator::new(map);
            let enabled_variant = evaluator.enabled(item);
            let is_enabled = enabled_variant.map(Variant::into).unwrap_or(false);

            // If the item is not enabled, all the configuration values set by externally
            // are to be discarded.
            if !is_enabled {
                for value in item.config_items_mut().values_mut() {
                    value.reset_all();
                }
            } else {
                if let Some(current_config_item) =
                    Self::cloned_int_current_config_item(item.clone(), &evaluator)
                {
                    let selects_me = item.selects_me();
                    let imply_me = item.imply_me();

                    for (_, config_item) in item.config_items_mut() {
                        let is_current = config_item == &current_config_item;
                        let default_value =
                            evaluator.config_item_default_value(&current_config_item);
                        match config_item.choice_mode() {
                            None => {
                                // If the choice mode is none, then the value can not be set by
                                // the select and/or imply methods, therefore, it is only verified
                                // against it's default value. If it is the same as the default
                                // value, the value is to be reset, otherwise it is to be set.
                                if is_current {
                                    if let Some(value) = config_item.config_value() {
                                        if value != default_value {
                                            config_item.set_value(value);
                                        } else {
                                            config_item.reset_value();
                                        }
                                    } else {
                                        config_item.reset_value();
                                    }
                                    config_item.reset_config_value()
                                } else {
                                    config_item.reset_all()
                                }
                            }
                            Some(_) => {
                                if is_current {
                                    let maybe_selects_value =
                                        Self::get_highest_reverse_dependency_value(
                                            &evaluator,
                                            &selects_me,
                                        );
                                    let maybe_implies_value =
                                        Self::get_highest_reverse_dependency_value(
                                            &evaluator, &imply_me,
                                        );

                                    // If a value is selected, then the selected value is the minimum value
                                    // which can be used by the item. Thus, if the value of the selecting
                                    // object is m, then the minimum value of the selected value is also m,
                                    // any value lower than m, is in error.

                                    // If a value is implied, then the same rules apply, just only to the
                                    // default value. The value can however be overridden completely.

                                    match (maybe_selects_value, maybe_implies_value) {
                                        (Some(selects_value), _) => {
                                            // Though the item is selected, the value is that of the implication,
                                            // if the implication is higher.
                                            config_item.set_choice_mode(
                                                true,
                                                selects_value.clone().into(),
                                            );

                                            // if the config value > the select value, then retain the
                                            // choice value, otherwise, remove it.
                                            if let Some(config_value) = config_item.config_value() {
                                                if config_value < selects_value {
                                                    config_item.reset_value();
                                                } else {
                                                    let default_value = evaluator
                                                        .config_item_default_value(config_item);
                                                    if config_value != default_value {
                                                        config_item.set_value(config_value);
                                                    } else {
                                                        config_item.reset_value();
                                                    }
                                                }
                                            }
                                            config_item.reset_config_value();
                                        }
                                        (_, Some(implies_value)) => {
                                            // If a value is explicitly set, and is not equal to the lower bound nor the
                                            // default value, then set the value explicitly.
                                            let default_value =
                                                evaluator.config_item_default_value(config_item);

                                            config_item.set_choice_mode(
                                                false,
                                                implies_value.clone().into(),
                                            );

                                            if let Some(config_value) = config_item.config_value() {
                                                if config_value != default_value
                                                    && config_value != implies_value
                                                {
                                                    config_item.set_value(config_value);
                                                } else {
                                                    config_item.reset_value();
                                                }
                                            }
                                            config_item.reset_config_value();
                                        }
                                        (_, None) => {
                                            let default_value =
                                                evaluator.config_item_default_value(config_item);
                                            config_item.set_choice_mode(false, Tristate::FALSE);
                                            if let Some(config_value) = config_item.config_value() {
                                                if config_value != default_value {
                                                    config_item.set_value(config_value);
                                                } else {
                                                    config_item.reset_value();
                                                }
                                            }
                                            config_item.reset_config_value();
                                        }
                                    }
                                } else {
                                    config_item.reset_all()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fn take_available_snapshot(
        snapshots: &HashMap<ItemReference, HashSet<ItemReference>>,
    ) -> Option<ItemReference> {
        let keys: HashSet<ItemReference> = snapshots.keys().cloned().collect();
        for (snapshot_itemref, snapshot_dependencies) in snapshots {
            let mut resolved = true;
            for dep in snapshot_dependencies {
                if keys.contains(dep) {
                    resolved = false;
                    break;
                }
            }
            if resolved {
                return Some(snapshot_itemref.clone());
            }
        }
        None
    }

    fn get_highest_reverse_dependency_value(
        evaluator: &ExpensiveEvaluator,
        revdeps: &HashMap<String, Option<ResolvedExpr>>,
    ) -> Option<Variant> {
        let itemrefs = evaluator.dependency_itemrefs(revdeps);

        if itemrefs.len() > 0 {
            let mut highest_value = Variant::from(false);
            for itemref in itemrefs {
                let value = evaluator.current_value(&itemref);
                if let Some(value) = value {
                    if value > highest_value {
                        highest_value = value;
                    }
                }
            }
            Some(highest_value)
        } else {
            None
        }
    }

    /// Writes the current configuration known to the configuration registry
    /// into a file, typically this file has the file name ".config"
    pub fn write_dotconfig_file(&self, dotconfig_filename: &str) -> Result<(), Error> {
        let mut f = OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .truncate(true)
            .open(dotconfig_filename)?;
        f.write("#\n".as_bytes())?;
        f.write("# Automatically generated file; DO NOT EDIT\n".as_bytes())?;
        match self.write_dotconfig(&mut f) {
            Ok(_) => Ok(()),
            Err(e) => Err(Error::from(format!(
                "Saving configuration to: {} failed - {}",
                dotconfig_filename, e
            ))),
        }
    }

    /// Writes the current configuration known to the configuration registry
    /// into a Write trait supporting type.
    pub fn write_dotconfig(&self, output: &mut dyn Write) -> Result<(), Error> {
        output.write("# ".as_bytes())?;
        output.write(self.main_menu_name.to_string().as_bytes())?;
        output.write("\n#\n\n".as_bytes())?;
        for (itemref, _) in self.items() {
            match itemref {
                ItemReference::Config(name) | ItemReference::MenuConfig(name) => {
                    match self.current_value(itemref) {
                        Some(value) => {
                            output.write(name.to_string().as_bytes())?;
                            output.write("=".as_bytes())?;
                            output.write(value.dot_config().as_bytes())?;
                        }
                        None => {
                            output.write("#".as_bytes())?;
                            output.write(name.as_bytes())?;
                            output.write("=is not set".as_bytes())?;
                        }
                    };
                    output.write("\n".as_bytes())?;
                }
                _ => (),
            }
        }

        Ok(())
    }

    /// Reads the saved configuration from a file, typically this file has
    /// the file name ".config", and mutates the configuration registry
    /// accordingly.
    pub fn read_dotconfig_file(&mut self, dotconfig_filename: &str) -> Result<(), LoadError> {
        let mut f = match OpenOptions::new().read(true).open(dotconfig_filename) {
            Ok(f) => f,
            Err(e) => {
                return Err(LoadError::from(Error::from(format!(
                    "⛔ Can not read {} - {}",
                    dotconfig_filename, e
                ))));
            }
        };

        self.read_dotconfig(&mut f)
    }

    /// Reads the saved configuration from a Read trait supporting type,
    /// and mutates the configuration registry accordingly.
    pub fn read_dotconfig(&mut self, input: &mut dyn Read) -> Result<(), LoadError> {
        let reader = BufReader::new(input);
        let lines = reader.lines();
        for (_, item) in self.items.iter_mut() {
            for (_, config_item) in item.config_items_mut() {
                config_item.reset_value();
            }
        }
        let result = self.read_dotconfig_by_line(lines);
        result
    }

    fn read_dotconfig_by_line(
        &mut self,
        lines: Lines<BufReader<&mut dyn Read>>,
    ) -> Result<(), LoadError> {
        let mut invalid_lines: Vec<(String, String)> = Vec::new();

        for line in lines {
            // Get the actual content of the line or return an error
            let line = line?;

            // Trim the line such that it begins with a valid character
            let line = line.trim();

            // If the line is empty, skip it
            if line.is_empty() {
                continue;
            }

            // If the line starts with a pund/hash symbol, it is a comment
            // skip it.
            if line.starts_with('#') {
                continue;
            }

            // LHS receives the name of the descriptor and RHS receives
            // the actual value.
            let mut lhs = String::new();
            let mut rhs = String::new();
            let mut invalid = false;
            let mut finished = false;
            let mut in_string = false;
            let mut escape = false;

            // Altough reading a line might fail, the process will continue
            // and simply skip the current value.
            #[derive(Debug)]
            enum MachineState {
                Start,
                Lhs,
                Assign,
                StartRhs,
                Rhs,
                End,
            }

            let mut state = MachineState::Start;
            for c in line.chars() {
                if !invalid && !finished {
                    match state {
                        MachineState::Start => {
                            if !c.is_whitespace() && c != '=' && c != '#' {
                                state = MachineState::Lhs;
                                lhs.push(c);
                            } else if c.is_whitespace() {
                                // Skip the current value
                            } else {
                                // This line is illegal
                                invalid = true;
                            }
                        }
                        MachineState::Lhs => {
                            if !c.is_whitespace() && c != '=' {
                                lhs.push(c);
                            } else if c.is_whitespace() {
                                state = MachineState::Assign;
                            } else if c == '=' {
                                state = MachineState::StartRhs;
                            } else {
                                invalid = true;
                            }
                        }
                        MachineState::Assign => {
                            if c.is_whitespace() {
                                // Skip the current value
                            } else if c == '=' {
                                state = MachineState::StartRhs;
                            } else {
                                invalid = true;
                            }
                        }
                        MachineState::StartRhs => {
                            if !c.is_whitespace() && c != '#' {
                                state = MachineState::Rhs;
                                if c == '"' {
                                    in_string = true;
                                }
                                rhs.push(c);
                            } else if c.is_whitespace() {
                                // Skip the current value
                            } else {
                                // The value is empty
                                finished = true;
                            }
                        }
                        MachineState::Rhs => {
                            if c == '"' {
                                if in_string && !escape {
                                    in_string = false;
                                    rhs.push(c);
                                } else if in_string && escape {
                                    rhs.push(c);
                                } else if !in_string {
                                    in_string = true;
                                    rhs.push(c);
                                } else {
                                    invalid = true;
                                }
                            } else if c == '\\' {
                                if escape && in_string {
                                    rhs.push(c);
                                    escape = false;
                                } else if in_string {
                                    escape = true;
                                } else {
                                    rhs.push(c);
                                }
                            } else if !c.is_whitespace() && !in_string && c != '#' || in_string {
                                rhs.push(c);
                            } else if c.is_whitespace() {
                                state = MachineState::End;
                            } else if c == '#' {
                                finished = true;
                            } else {
                                invalid = true;
                            }
                        }
                        MachineState::End => {
                            if c.is_whitespace() {
                                // Ignore
                            } else if c == '#' {
                                finished = true;
                            } else {
                                invalid = true;
                            }
                        }
                    }
                }
            }

            if !escape && !invalid {
                let value = Variant::from_dot_config(&rhs);
                match self.find_itemref(&lhs) {
                    Some(itemref) => match self.int_set_value(&itemref, value) {
                        Ok(_) => (),
                        Err(e) => invalid_lines.push((e.to_string(), line.to_string())),
                    },
                    None => {
                        invalid_lines
                            .push((format!("Invalid or unknown item: {lhs}"), line.to_string()));
                    }
                }
            } else {
                invalid_lines.push(("Invalid syntax".to_owned(), line.to_string()));
            }
        }

        self.reset()?;

        match invalid_lines.is_empty() {
            true => Ok(()),
            false => Err(LoadError::from(Error::from(invalid_lines))),
        }
    }
}
