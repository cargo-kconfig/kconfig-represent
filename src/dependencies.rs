/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//! This module contains the resolve logic, which uses the configuration registry,
//! it's items and it's evaluator to resolve dependencies.

use std::collections::{BTreeSet, HashMap, HashSet};
use std::fmt::Display;

use kconfig_parser::{
    ast::{structs::Expr, Dependency, Element},
    Symbol,
};

use crate::{Item, ItemReference};

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct ForwardDependency {
    forward_name: String,
    maybe_condition: Option<Dependency>,
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct BackwardDependency {
    backward_name: String,
    maybe_condition: Option<Dependency>,
}

#[derive(Debug)]
pub(crate) struct SelectAndImplyDependencyResolver {
    forward_select: HashMap<String, HashSet<ForwardDependency>>,
    backward_select: HashMap<String, HashSet<BackwardDependency>>,
    forward_imply: HashMap<String, HashSet<ForwardDependency>>,
    backward_imply: HashMap<String, HashSet<BackwardDependency>>,
}

impl SelectAndImplyDependencyResolver {
    pub(crate) fn new() -> Self {
        Self {
            forward_select: HashMap::new(),
            backward_select: HashMap::new(),
            forward_imply: HashMap::new(),
            backward_imply: HashMap::new(),
        }
    }

    pub(crate) fn put_select(
        &mut self,
        itemref: &ItemReference,
        selected_name: &str,
        maybe_condition: &Option<Dependency>,
    ) {
        let forward_dependency = ForwardDependency {
            forward_name: selected_name.to_string(),
            maybe_condition: maybe_condition.clone(),
        };
        match self.forward_select.get_mut(&itemref.to_string()) {
            Some(value) => {
                value.insert(forward_dependency);
            }
            None => {
                let mut set = HashSet::new();
                set.insert(forward_dependency);
                self.forward_select.insert(itemref.to_string(), set);
            }
        }

        let backward_dependency = BackwardDependency {
            backward_name: itemref.to_string(),
            maybe_condition: maybe_condition.clone(),
        };
        match self.backward_select.get_mut(&selected_name.to_string()) {
            Some(value) => {
                value.insert(backward_dependency);
            }
            None => {
                let mut set = HashSet::new();
                set.insert(backward_dependency);
                self.backward_select.insert(selected_name.to_string(), set);
            }
        }
    }

    pub(crate) fn put_imply(
        &mut self,
        itemref: &ItemReference,
        implied_name: &str,
        maybe_condition: &Option<Dependency>,
    ) {
        let forward_dependency = ForwardDependency {
            forward_name: implied_name.to_string(),
            maybe_condition: maybe_condition.clone(),
        };
        match self.forward_imply.get_mut(&itemref.to_string()) {
            Some(value) => {
                value.insert(forward_dependency);
            }
            None => {
                let mut set = HashSet::new();
                set.insert(forward_dependency);
                self.forward_imply.insert(itemref.to_string(), set);
            }
        }
        let backward_dependency = BackwardDependency {
            backward_name: itemref.to_string(),
            maybe_condition: maybe_condition.clone(),
        };
        match self.backward_imply.get_mut(&implied_name.to_string()) {
            Some(value) => {
                value.insert(backward_dependency);
            }
            None => {
                let mut set = HashSet::new();
                set.insert(backward_dependency);
                self.backward_imply.insert(implied_name.to_string(), set);
            }
        }
    }
}

/// Dynamically resolves the states of menu and configuration items
/// discovered in the configuration registry by evaluating the AST each
/// cycle when the state of a single (configuration) item has been changed
/// through the user interface.
#[derive(Debug)]
pub(crate) struct DependencyResolver {
    select_and_imply_resolver: SelectAndImplyDependencyResolver,
    macro_symbols: HashSet<Symbol>,
}

impl DependencyResolver {
    /// Creates a new resolver instance for the duration of the existence of
    /// the given items. A resolver is constricted to the scope of the caller
    /// by it's given lifetime.
    pub(crate) fn new(
        select_and_imply_resolver: SelectAndImplyDependencyResolver,
        macro_symbols: HashSet<Symbol>,
    ) -> Self {
        Self {
            select_and_imply_resolver,
            macro_symbols,
        }
    }

    pub(crate) fn resolve(&mut self, items: &mut HashMap<ItemReference, Item>) {
        // Filter out all the known itemreferences, and store them into a HashMap, where individual
        // item references can be looked up, by name
        let mut lut: HashMap<String, ItemReference> = items
            .iter()
            .map(|(k, _)| (k.to_string(), k.clone()))
            .collect();

        let itemrefs: HashSet<ItemReference> = items.keys().cloned().collect();

        for (_, item) in items.iter_mut() {
            if let Some(elt) = item.raw_elt() {
                let item_types = elt.types().cloned();
                let maybe_defaults = elt.defaults().cloned();
                let itemref = &item.itemref();
                item.set_dependencies(self.resolve_dependencies(elt, &item, &itemrefs, &mut lut));
                item.set_selects_me(self.resolve_select(&itemref, &itemrefs, &mut lut));
                item.set_imply_me(self.resolve_imply(&itemref, &itemrefs, &mut lut));
                item.set_selected(self.resolve_selected(&itemref, &itemrefs, &mut lut));
                item.set_implied(self.resolve_implied(&itemref, &itemrefs, &mut lut));
                self.resolve_config_dependencies(item_types, item, &itemrefs, &mut lut);
                self.resolve_default_expressions(maybe_defaults, &itemrefs, &mut lut, item);
            }
        }
    }

    fn resolve_dependencies(
        &self,
        elt: &Element,
        item: &Item,
        itemrefs: &HashSet<ItemReference>,
        lut: &mut HashMap<String, ItemReference>,
    ) -> HashSet<ItemDependency> {
        let mut set: HashSet<ItemDependency> = HashSet::new();

        if let Some(parent) = item.parent() {
            set.insert(ItemDependency::Heavy(ResolvedExpr::new(
                &Expr::Sym(parent.clone()),
                itemrefs,
                &self.macro_symbols,
                lut,
            )));
        }

        let dependencies: HashSet<ItemDependency> = elt
            .dependencies()
            .iter()
            .map(|d| d.expr())
            .map(|e| ResolvedExpr::new(&e, itemrefs, &self.macro_symbols, lut))
            .map(|re| ItemDependency::Normal(re))
            .collect();

        let conditions: HashSet<ItemDependency> = item
            .conditions()
            .iter()
            .map(|d| d.expr())
            .map(|e| ResolvedExpr::new(&e, itemrefs, &self.macro_symbols, lut))
            .map(|re| ItemDependency::Trivial(re))
            .collect();

        set.extend(dependencies);
        set.extend(conditions);

        set
    }

    fn resolve_select(
        &self,
        itemref: &ItemReference,
        itemrefs: &HashSet<ItemReference>,
        lut: &mut HashMap<String, ItemReference>,
    ) -> HashMap<String, Option<ResolvedExpr>> {
        match self
            .select_and_imply_resolver
            .backward_select
            .get(&itemref.to_string())
        {
            Some(s) => {
                let mut map = HashMap::new();
                for dep in s {
                    let name = dep.backward_name.clone();
                    match &dep.maybe_condition {
                        Some(dep) => map.insert(
                            name,
                            Some(ResolvedExpr::new(
                                &dep.expr(),
                                itemrefs,
                                &self.macro_symbols,
                                lut,
                            )),
                        ),
                        None => map.insert(name, None),
                    };
                }
                map
            }
            None => HashMap::new(),
        }
    }

    fn resolve_selected(
        &self,
        itemref: &ItemReference,
        itemrefs: &HashSet<ItemReference>,
        lut: &mut HashMap<String, ItemReference>,
    ) -> HashMap<String, Option<ResolvedExpr>> {
        match self
            .select_and_imply_resolver
            .forward_select
            .get(&itemref.to_string())
        {
            Some(s) => {
                let mut map = HashMap::new();
                for dep in s {
                    let name = dep.forward_name.clone();
                    match &dep.maybe_condition {
                        Some(dep) => map.insert(
                            name,
                            Some(ResolvedExpr::new(
                                &dep.expr(),
                                itemrefs,
                                &self.macro_symbols,
                                lut,
                            )),
                        ),
                        None => map.insert(name, None),
                    };
                }
                map
            }
            None => HashMap::new(),
        }
    }

    fn resolve_imply(
        &self,
        itemref: &ItemReference,
        itemrefs: &HashSet<ItemReference>,
        lut: &mut HashMap<String, ItemReference>,
    ) -> HashMap<String, Option<ResolvedExpr>> {
        match self
            .select_and_imply_resolver
            .backward_imply
            .get(&itemref.to_string())
        {
            Some(s) => {
                let mut map = HashMap::new();
                for dep in s {
                    let name = dep.backward_name.clone();
                    match &dep.maybe_condition {
                        Some(dep) => map.insert(
                            name,
                            Some(ResolvedExpr::new(
                                &dep.expr(),
                                itemrefs,
                                &self.macro_symbols,
                                lut,
                            )),
                        ),
                        None => map.insert(name, None),
                    };
                }
                map
            }
            None => HashMap::new(),
        }
    }

    fn resolve_implied(
        &self,
        itemref: &ItemReference,
        itemrefs: &HashSet<ItemReference>,
        lut: &mut HashMap<String, ItemReference>,
    ) -> HashMap<String, Option<ResolvedExpr>> {
        match self
            .select_and_imply_resolver
            .forward_imply
            .get(&itemref.to_string())
        {
            Some(s) => {
                let mut map = HashMap::new();
                for dep in s {
                    let name = dep.forward_name.clone();
                    match &dep.maybe_condition {
                        Some(dep) => map.insert(
                            name,
                            Some(ResolvedExpr::new(
                                &dep.expr(),
                                itemrefs,
                                &self.macro_symbols,
                                lut,
                            )),
                        ),
                        None => map.insert(name, None),
                    };
                }
                map
            }
            None => HashMap::new(),
        }
    }

    fn resolve_config_dependencies(
        &mut self,
        item_types: Option<HashMap<kconfig_parser::ast::ConfigType, Option<Dependency>>>,
        item: &mut Item,
        itemrefs: &HashSet<ItemReference>,
        lut: &mut HashMap<String, ItemReference>,
    ) {
        if let Some(types) = item_types {
            for (config_type, dependency) in types {
                if let Some(dependency) = dependency {
                    if let Some(config_item) = item.config_items_mut().get_mut(&config_type) {
                        let expr = ResolvedExpr::new(
                            &dependency.expr(),
                            itemrefs,
                            &self.macro_symbols,
                            lut,
                        );
                        config_item.set_dependencies(expr)
                    }
                }
            }
        }
    }

    fn resolve_default_expressions(
        &mut self,
        maybe_defaults: Option<HashMap<Expr, Option<Dependency>>>,
        itemrefs: &HashSet<ItemReference>,
        lut: &mut HashMap<String, ItemReference>,
        item: &mut Item,
    ) {
        if let Some(defaults) = maybe_defaults {
            for (expr, maybe_dependency) in defaults {
                let default_expr = ResolvedExpr::new(&expr, itemrefs, &self.macro_symbols, lut);

                let maybe_expr = match maybe_dependency {
                    Some(dependency) => Some(ResolvedExpr::new(
                        &dependency.expr(),
                        itemrefs,
                        &self.macro_symbols,
                        lut,
                    )),
                    None => None,
                };

                for (_, config_item) in item.config_items_mut().iter_mut() {
                    config_item.add_default(
                        default_expr.clone(),
                        match &maybe_expr {
                            Some(v) => Some(v.clone()),
                            None => None,
                        },
                    );
                }
            }
        }
    }
}

/// Value indicating a resolved expression, which can be evaluated directly.
/// It knows which item references are dependencies, and is therefore able to
/// discover the resolution path of an evaluation.
#[derive(Hash, Clone, PartialEq, Eq, Debug)]
pub struct ResolvedExpr {
    item_expr: ItemExpr,
    itemrefs: BTreeSet<ItemReference>,
}

impl ResolvedExpr {
    pub fn new(
        expr: &Expr,
        itemrefs: &HashSet<ItemReference>,
        macro_symbols: &HashSet<Symbol>,
        lut: &mut HashMap<String, ItemReference>,
    ) -> ResolvedExpr {
        let expanded_symbols: HashMap<String, String> = macro_symbols
            .iter()
            .map(|s| (s.name(), s.value()))
            .collect();

        let item_expr = ItemExpr::resolve(expr, itemrefs, &expanded_symbols, lut);
        let itemrefs = item_expr.itemrefs();

        Self {
            item_expr,
            itemrefs: itemrefs,
        }
    }

    /// Returns the actual expressions underpinning the resolved expression, which
    /// acts as a container for both the resolved item expressions, as well as a
    /// direct reference to all item references within the underlying item
    /// expressions.
    pub fn item_expr<'a>(&'a self) -> &'a ItemExpr {
        &self.item_expr
    }

    /// Returns the item references used by the given item expression
    pub fn itemrefs<'a>(&'a self) -> &'a BTreeSet<ItemReference> {
        &self.itemrefs
    }
}

/// Wrapper around an expression, resolved into the macro values, and
/// item references.
#[derive(PartialEq, Eq, Debug, Clone, PartialOrd, Ord, Hash)]
pub enum ItemExpr {
    /// A value, typically the result of a resolved symbol to a macro
    Value(String),

    /// An item reference, typically the result of a resolved symbol to
    /// an item reference.
    ItemReference(ItemReference),

    /// An equals expression
    Eq(BTreeSet<ItemExpr>),

    /// A Not equals expression
    Ne(BTreeSet<ItemExpr>),

    /// A Lesser than expression
    Lt(BTreeSet<ItemExpr>),

    /// A Greater than expression
    Gt(BTreeSet<ItemExpr>),

    /// A Lesser than or equal expression
    Lte(BTreeSet<ItemExpr>),

    /// A Greater than or equal expression
    Gte(BTreeSet<ItemExpr>),

    /// An and expression
    And(BTreeSet<ItemExpr>),

    /// An or expression
    Or(BTreeSet<ItemExpr>),

    /// A subexpression, typically just parentheses surrounding the expression
    Sub(Box<ItemExpr>),

    /// A negation expression
    Not(Box<ItemExpr>),

    /// Could not resolve value
    Unresolved(String),
}
impl ItemExpr {
    fn resolve(
        expr: &Expr,
        refs: &HashSet<ItemReference>,
        syms: &HashMap<String, String>,
        cache: &mut HashMap<String, ItemReference>,
    ) -> ItemExpr {
        match expr {
            Expr::Sym(s) => Self::cached_sym_to_item_expr(syms, s, cache, refs),
            Expr::Eq(exprs) => ItemExpr::Eq(Self::expr_to_item_expr(exprs, refs, syms, cache)),
            Expr::Ne(exprs) => ItemExpr::Ne(Self::expr_to_item_expr(exprs, refs, syms, cache)),
            Expr::Lt(exprs) => ItemExpr::Lt(Self::expr_to_item_expr(exprs, refs, syms, cache)),
            Expr::Gt(exprs) => ItemExpr::Gt(Self::expr_to_item_expr(exprs, refs, syms, cache)),
            Expr::Lte(exprs) => ItemExpr::Lte(Self::expr_to_item_expr(exprs, refs, syms, cache)),
            Expr::Gte(exprs) => ItemExpr::Gte(Self::expr_to_item_expr(exprs, refs, syms, cache)),
            Expr::And(exprs) => ItemExpr::And(Self::expr_to_item_expr(exprs, refs, syms, cache)),
            Expr::Or(exprs) => ItemExpr::Or(Self::expr_to_item_expr(exprs, refs, syms, cache)),
            Expr::Sub(child) => ItemExpr::Sub(Box::new(Self::resolve(child, refs, syms, cache))),
            Expr::Not(child) => ItemExpr::Not(Box::new(Self::resolve(child, refs, syms, cache))),
        }
    }

    fn cached_sym_to_item_expr(
        syms: &HashMap<String, String>,
        s: &String,
        cache: &mut HashMap<String, ItemReference>,
        refs: &HashSet<ItemReference>,
    ) -> ItemExpr {
        if let Some(v) = syms.get(s) {
            ItemExpr::Value(v.clone())
        } else {
            if let Some(v) = cache.get(s) {
                ItemExpr::ItemReference(v.clone())
            } else {
                if let Some(v) = refs.get(&ItemReference::Menu(s.clone())) {
                    cache.insert(s.clone(), v.clone());
                    ItemExpr::ItemReference(v.clone())
                } else {
                    if let Some(v) = refs.get(&ItemReference::MenuConfig(s.clone())) {
                        cache.insert(s.clone(), v.clone());
                        ItemExpr::ItemReference(v.clone())
                    } else {
                        if let Some(v) = refs.get(&ItemReference::Config(s.clone())) {
                            cache.insert(s.clone(), v.clone());
                            ItemExpr::ItemReference(v.clone())
                        } else {
                            ItemExpr::Unresolved(s.clone())
                        }
                    }
                }
            }
        }
    }

    fn expr_to_item_expr(
        exprs: &BTreeSet<Expr>,
        refs: &HashSet<ItemReference>,
        syms: &HashMap<String, String>,
        cache: &mut HashMap<String, ItemReference>,
    ) -> BTreeSet<ItemExpr> {
        exprs
            .iter()
            .map(|e| Self::resolve(e, refs, syms, cache))
            .collect()
    }

    fn itemrefs(&self) -> BTreeSet<ItemReference> {
        let mut set: BTreeSet<ItemReference> = BTreeSet::new();

        match self {
            ItemExpr::Value(_) | ItemExpr::Unresolved(_) => set,
            ItemExpr::ItemReference(itemref) => {
                set.insert(itemref.clone());
                set
            }
            ItemExpr::Eq(exprs)
            | ItemExpr::Ne(exprs)
            | ItemExpr::Lt(exprs)
            | ItemExpr::Gt(exprs)
            | ItemExpr::Lte(exprs)
            | ItemExpr::Gte(exprs)
            | ItemExpr::And(exprs)
            | ItemExpr::Or(exprs) => {
                for r in exprs.iter().map(|s| s.itemrefs()) {
                    set.extend(r)
                }
                set
            }
            ItemExpr::Sub(expr) | ItemExpr::Not(expr) => {
                set.extend(expr.itemrefs());
                set
            }
        }
    }
}

impl Display for ItemExpr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Self::Value(s) => f.write_str(s)?,
            Self::ItemReference(itemref) => {
                f.write_str("&")?;
                match itemref {
                    ItemReference::Config(s) => {
                        f.write_str("config => ")?;
                        f.write_str(s)?
                    }
                    ItemReference::Menu(s) => {
                        f.write_str("menu => ")?;
                        f.write_str(s)?
                    }
                    ItemReference::MenuConfig(s) => {
                        f.write_str("menuconfig => ")?;
                        f.write_str(s)?
                    }
                }
            }

            Self::Eq(exprs) => {
                f.write_str("eq {")?;
                for expr in exprs {
                    expr.fmt(f)?;
                }
                f.write_str("}")?;
            }

            Self::Ne(exprs) => {
                f.write_str("ne {")?;
                for expr in exprs {
                    expr.fmt(f)?;
                }
                f.write_str("}")?;
            }

            Self::Lt(exprs) => {
                f.write_str("lt {")?;
                for expr in exprs {
                    expr.fmt(f)?;
                }
                f.write_str("}")?;
            }

            Self::Gt(exprs) => {
                f.write_str("gt {")?;
                for expr in exprs {
                    expr.fmt(f)?;
                }
                f.write_str("}")?;
            }

            Self::Lte(exprs) => {
                f.write_str("lte {")?;
                for expr in exprs {
                    expr.fmt(f)?;
                }
                f.write_str("}")?;
            }

            Self::Gte(exprs) => {
                f.write_str("gte {")?;
                for expr in exprs {
                    expr.fmt(f)?;
                }
                f.write_str("}")?;
            }

            Self::And(exprs) => {
                f.write_str("and {")?;
                for expr in exprs {
                    expr.fmt(f)?;
                }
                f.write_str("}")?;
            }

            Self::Or(exprs) => {
                f.write_str("or {")?;
                for expr in exprs {
                    expr.fmt(f)?;
                }
                f.write_str("}")?;
            }

            Self::Sub(expr) => {
                f.write_str("(")?;
                expr.fmt(f)?;
                f.write_str(")")?;
            }

            Self::Not(expr) => {
                f.write_str("not (")?;
                expr.fmt(f)?;
                f.write_str(")")?;
            }

            Self::Unresolved(s) => {
                f.write_str("#")?;
                f.write_str(s)?
            }
        };

        Ok(())
    }
}

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum ItemDependency {
    Heavy(ResolvedExpr),
    Normal(ResolvedExpr),
    Trivial(ResolvedExpr),
}

impl ItemDependency {
    pub fn expr<'a>(&'a self) -> &'a ResolvedExpr {
        match self {
            ItemDependency::Heavy(expr)
            | ItemDependency::Normal(expr)
            | ItemDependency::Trivial(expr) => expr,
        }
    }
}
