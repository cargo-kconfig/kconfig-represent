/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//! This module enables evaluation of expression statements, such as
//! depends on expressions, if expressions, etc.

use crate::variant::VariantDataType;
use crate::Variant;
use crate::{dependencies::ResolvedExpr, item::ConfigItem};
use std::collections::{BTreeSet, HashMap};

use crate::{dependencies::ItemExpr, Error, Item, ItemReference, Tristate};
use kconfig_parser::ast::ConfigType;

/// Implements the evaluator for a particular configuration registry, evaluating
/// whether items are enabled and the values thereof
#[derive(Debug)]
pub(crate) struct Evaluator<'a> {
    items: &'a HashMap<ItemReference, Item>,
}

impl<'a> Evaluator<'a> {
    pub(crate) fn new(items: &'a HashMap<ItemReference, Item>) -> Self {
        Self { items }
    }

    /// If the item has no dependencies, then the item is enabled. If the
    /// item does have dependencies, then the dependency's value needs to
    /// evaluate to true and recurively their depenendencies also need to
    /// evaluate to an enabled state. A dependency should not be circular,
    /// this will result into an error, and dependencies should exist,
    /// otherwise, they also are in error.
    pub(crate) fn enabled(&self, item: &Item) -> Result<Variant, Error> {
        let mut result = Variant::from(Tristate::TRUE);
        for dep in item.item_dependencies() {
            let evaled = self.evalexpr(dep.expr().item_expr())?;
            let eval_datatype = evaled.datatype();
            if eval_datatype == VariantDataType::Bool || eval_datatype == VariantDataType::Tristate
            {
                if evaled < result {
                    result = evaled;
                }
            } else {
                result = Variant::from(Tristate::FALSE);
            }
        }
        // If no further false resulting dependencies are available, return true
        // as an Ok result.
        Ok(result)
    }

    pub(crate) fn evalexpr(&self, expr: &ItemExpr) -> Result<Variant, Error> {
        match expr {
            ItemExpr::Value(value) => Ok(Variant::parse(value)),
            ItemExpr::ItemReference(itemref) => {
                let item = self
                    .items
                    .get(itemref)
                    .ok_or(format!("Item reference invalid {itemref}"))?;
                if self.enabled(item)?.into() {
                    for config_item in item.config_items().values() {
                        let fallback_default =
                            Variant::new_typed(&VariantDataType::from(&config_item.config_type()));
                        let maybe_dependencies = config_item.maybe_dependencies();
                        if let Some(dependencies) = maybe_dependencies {
                            if self.evalexpr(dependencies.item_expr())?.into() {
                                let result = config_item.value();
                                return match result {
                                    Some(value) => Ok(value.clone()),
                                    None => Ok(self
                                        .resolve_default(config_item)
                                        .unwrap_or(fallback_default)),
                                };
                            }
                        } else {
                            let result = config_item.value();
                            return match result {
                                Some(value) => Ok(value.clone()),
                                None => Ok(self
                                    .resolve_default(config_item)
                                    .unwrap_or(fallback_default)),
                            };
                        }
                    }
                    // If the option is a menu, then this will be true, otherwise, it
                    // will be false, as no configuration item is present.
                    match itemref {
                        ItemReference::Menu(_) => Ok(Variant::from(true)),
                        _ => Ok(Variant::from(false)),
                    }
                } else {
                    Ok(Variant::from(false))
                }
            }
            ItemExpr::Eq(items) => Ok(self.eq_compare(items)?),
            ItemExpr::Ne(items) => Ok(self.ne_compare(items)?),
            ItemExpr::Lt(items) => Ok(self.lt_compare(items)?),
            ItemExpr::Gt(items) => Ok(self.gt_compare(items)?),
            ItemExpr::Lte(items) => Ok(self.lte_compare(items)?),
            ItemExpr::Gte(items) => Ok(self.gte_compare(items)?),
            ItemExpr::And(items) => Ok(self.and_compare(items)?),
            ItemExpr::Or(items) => Ok(self.or_compare(items)?),
            ItemExpr::Sub(item) => self.evalexpr(item.as_ref()),
            ItemExpr::Not(item) => Ok(!(self.evalexpr(item.as_ref())?)),
            ItemExpr::Unresolved(value) => {
                // If the item expression can not be resolved, interpret it in the
                // most logical fashion
                Ok(Variant::parse(value))
            }
        }
    }

    pub(crate) fn dependency_itemrefs(
        &self,
        revdeps: &HashMap<String, Option<ResolvedExpr>>,
    ) -> BTreeSet<ItemReference> {
        let mut result = BTreeSet::new();
        for (name, maybe_depexpr) in revdeps {
            let maybe_name = match maybe_depexpr {
                Some(depexpr) => {
                    let evaluated: bool = self
                        .evalexpr(depexpr.item_expr())
                        .unwrap_or(Variant::from(false))
                        .into();
                    if evaluated {
                        Some(name)
                    } else {
                        None
                    }
                }
                None => Some(name),
            };

            if let Some(name) = maybe_name {
                for (itemref, _) in self.items {
                    if &itemref.to_string() == name {
                        result.insert(itemref.clone());
                    }
                }
            }
        }

        result
    }

    pub(crate) fn config_item_default_value(&self, config_item: &ConfigItem) -> Variant {
        let mut default: Option<Variant> = None;
        for (default_expr, maybe_dep_expr) in config_item.defaults() {
            if default == None {
                match maybe_dep_expr {
                    Some(dep_expr) => {
                        if let Ok(variant) = self.evalexpr(dep_expr.item_expr()) {
                            let enabled: bool = variant.into();
                            if enabled {
                                if let Ok(variant) = self.evalexpr(default_expr.item_expr()) {
                                    default = Some(variant)
                                }
                            }
                        }
                    }
                    None => {
                        let resolution = self.evalexpr(default_expr.item_expr());
                        if let Ok(variant) = resolution {
                            default = Some(variant)
                        }
                    }
                }
            } else {
                break;
            }
        }
        let config_type = config_item.config_type();
        let datatype = VariantDataType::from(&config_type);
        match default {
            Some(value) => value.convert_into(&datatype),
            None => Variant::new_typed(&datatype),
        }
    }

    fn int_current_config_item(&self, item: &'a Item) -> Option<&'a ConfigItem> {
        let mut restricted_config_item: Option<&ConfigItem> = None;
        let mut unrestricted_config_item: Option<&ConfigItem> = None;

        for (_, config_item) in item.config_items() {
            if let Some(expr) = config_item.maybe_dependencies() {
                if let Ok(variant) = self.evalexpr(expr.item_expr()) {
                    let enabled: bool = variant.into();
                    if enabled {
                        restricted_config_item = Some(config_item);
                    }
                }
            } else {
                unrestricted_config_item = Some(config_item);
            }
        }

        match (restricted_config_item, unrestricted_config_item) {
            (Some(config_item), _) => Some(&config_item),
            (_, Some(config_item)) => Some(&config_item),
            _ => None,
        }
    }

    fn default_or_choice_low(&self, config_item: &ConfigItem) -> Variant {
        let default_value = self.config_item_default_value(&config_item);
        let choice_low = config_item.choice_low();
        let value = if choice_low > default_value {
            choice_low
        } else {
            default_value
        };

        if let ConfigType::Bool = config_item.config_type() {
            let temp: bool = value.into();
            Variant::from(temp)
        } else {
            value
        }
    }

    pub(crate) fn current_value(&self, itemref: &ItemReference) -> Option<Variant> {
        self.items
            .get(itemref)
            .map(|item| self.int_current_config_item(item))
            .flatten()
            .map(|config_item| match config_item.choice_mode() {
                Some(_) => match config_item.value() {
                    Some(value) => value,
                    None => self.default_or_choice_low(&config_item),
                },
                None => match config_item.value() {
                    Some(value) => value,
                    None => self.config_item_default_value(&config_item),
                },
            })
    }

    fn resolve_default(&self, config_item: &ConfigItem) -> Option<Variant> {
        for (default_expr, maybe_dep_expr) in config_item.defaults() {
            match maybe_dep_expr {
                Some(dep_expr) => {
                    if self
                        .evalexpr(dep_expr.item_expr())
                        .unwrap_or(Variant::from(false))
                        .into()
                    {
                        if let Ok(result) = self.evalexpr(default_expr.item_expr()) {
                            return Some(
                                result.transform(VariantDataType::from(&config_item.config_type())),
                            );
                        }
                    }
                }
                None => {
                    if let Ok(result) = self.evalexpr(default_expr.item_expr()) {
                        return Some(
                            result.transform(VariantDataType::from(&config_item.config_type())),
                        );
                    }
                }
            }
        }
        None
    }

    fn eq_compare(&self, exprs: &BTreeSet<ItemExpr>) -> Result<Variant, Error> {
        let mut first_result: Option<Variant> = None;
        for expr in exprs {
            if let Some(r) = &first_result {
                if self.evalexpr(expr)? != r.clone() {
                    return Ok(Variant::from(false));
                }
            } else {
                first_result = Some(self.evalexpr(expr)?);
            }
        }
        match first_result {
            Some(_) => Ok(Variant::from(true)),
            _ => Err(Error::from("Not enough statements to perform comparison")),
        }
    }

    fn ne_compare(&self, exprs: &BTreeSet<ItemExpr>) -> Result<Variant, Error> {
        let cmp = self.eq_compare(exprs)?;
        return Ok(!cmp);
    }

    fn lt_compare(&self, exprs: &BTreeSet<ItemExpr>) -> Result<Variant, Error> {
        let mut first_result: Option<Variant> = None;
        for expr in exprs {
            if let Some(r) = &first_result {
                if r.clone() < self.evalexpr(expr)? {
                    return Ok(Variant::from(true));
                } else {
                    return Ok(Variant::from(false));
                }
            } else {
                first_result = Some(self.evalexpr(expr)?);
            }
        }

        match first_result {
            Some(_) => Ok(Variant::from(true)),
            _ => Err(Error::from("Not enough statements to perform comparison")),
        }
    }

    fn gt_compare(&self, exprs: &BTreeSet<ItemExpr>) -> Result<Variant, Error> {
        let mut first_result: Option<Variant> = None;
        for expr in exprs {
            if let Some(r) = &first_result {
                if r.clone() > self.evalexpr(expr)? {
                    return Ok(Variant::from(true));
                } else {
                    return Ok(Variant::from(false));
                }
            } else {
                first_result = Some(self.evalexpr(expr)?);
            }
        }

        match first_result {
            Some(_) => Ok(Variant::from(true)),
            _ => Err(Error::from("Not enough statements to perform comparison")),
        }
    }

    fn lte_compare(&self, exprs: &BTreeSet<ItemExpr>) -> Result<Variant, Error> {
        let mut first_result: Option<Variant> = None;
        for expr in exprs {
            if let Some(r) = &first_result {
                if r.clone() <= self.evalexpr(expr)? {
                    return Ok(Variant::from(true));
                } else {
                    return Ok(Variant::from(false));
                }
            } else {
                first_result = Some(self.evalexpr(expr)?);
            }
        }

        match first_result {
            Some(_) => Ok(Variant::from(true)),
            _ => Err(Error::from("Not enough statements to perform comparison")),
        }
    }

    fn gte_compare(&self, exprs: &BTreeSet<ItemExpr>) -> Result<Variant, Error> {
        let mut first_result: Option<Variant> = None;
        for expr in exprs {
            if let Some(r) = &first_result {
                if r.clone() >= self.evalexpr(expr)? {
                    return Ok(Variant::from(true));
                } else {
                    return Ok(Variant::from(false));
                }
            } else {
                first_result = Some(self.evalexpr(expr)?);
            }
        }

        match first_result {
            Some(_) => Ok(Variant::from(true)),
            _ => Err(Error::from("Not enough statements to perform comparison")),
        }
    }

    fn and_compare(&self, exprs: &BTreeSet<ItemExpr>) -> Result<Variant, Error> {
        let mut first_result: Option<Variant> = None;
        for expr in exprs {
            if let Some(r) = &first_result {
                if r.clone().into() && self.evalexpr(expr)?.into() {
                    return Ok(Variant::from(true));
                } else {
                    return Ok(Variant::from(false));
                }
            } else {
                first_result = Some(self.evalexpr(expr)?);
            }
        }

        match first_result {
            Some(_) => Ok(Variant::from(true)),
            _ => Err(Error::from("Not enough statements to perform comparison")),
        }
    }

    fn or_compare(&self, exprs: &BTreeSet<ItemExpr>) -> Result<Variant, Error> {
        let mut first_result: Option<Variant> = None;
        for expr in exprs {
            if let Some(r) = &first_result {
                if r.clone().into() || self.evalexpr(expr)?.into() {
                    return Ok(Variant::from(true));
                } else {
                    return Ok(Variant::from(false));
                }
            } else {
                first_result = Some(self.evalexpr(expr)?);
            }
        }

        match first_result {
            Some(_) => Ok(Variant::from(true)),
            _ => Err(Error::from("Not enough statements to perform comparison")),
        }
    }
}

pub(crate) struct ExpensiveEvaluator {
    items: HashMap<ItemReference, Item>,
}

impl ExpensiveEvaluator {
    pub(crate) fn new(items: HashMap<ItemReference, Item>) -> Self {
        Self { items }
    }

    pub(crate) fn enabled(&self, item: &Item) -> Result<Variant, Error> {
        Evaluator::new(&self.items).enabled(item)
    }

    pub(crate) fn evalexpr(&self, expr: &ItemExpr) -> Result<Variant, Error> {
        Evaluator::new(&self.items).evalexpr(expr)
    }

    pub(crate) fn dependency_itemrefs(
        &self,
        revdeps: &HashMap<String, Option<ResolvedExpr>>,
    ) -> BTreeSet<ItemReference> {
        Evaluator::new(&self.items).dependency_itemrefs(revdeps)
    }

    pub(crate) fn config_item_default_value(&self, config_item: &ConfigItem) -> Variant {
        Evaluator::new(&self.items).config_item_default_value(config_item)
    }

    pub(crate) fn current_value(&self, itemref: &ItemReference) -> Option<Variant> {
        Evaluator::new(&self.items).current_value(itemref)
    }
}
