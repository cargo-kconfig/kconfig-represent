/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//! This module implements a variant type, which encapsulates the known types
//! used by Kconfig, which are the bool, tristate, string, hex and int types.

use crate::Hex;
use crate::Tristate;
use kconfig_parser::ast::ConfigType;
use kconfig_parser::escape_string;
use kconfig_parser::parse_string;
use std::cmp::Ordering;
use std::fmt::Display;
use std::ops::Not;

/// An evaluated value represents the value of a an expression after evaluation.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct Variant {
    data: Data,
}

impl Variant {
    pub fn datatype(&self) -> VariantDataType {
        match &self.data {
            Data::String(_) => VariantDataType::String,
            Data::Bool(_) => VariantDataType::Bool,
            Data::Hex(_) => VariantDataType::Hex,
            Data::Int(_) => VariantDataType::Int,
            Data::Tristate(_) => VariantDataType::Tristate,
        }
    }

    pub fn new_typed(vdt: &VariantDataType) -> Self {
        Self {
            data: match vdt {
                &VariantDataType::String => Data::String(String::default()),
                &VariantDataType::Bool => Data::Bool(bool::default()),
                &VariantDataType::Hex => Data::Hex(Hex::default()),
                &VariantDataType::Int => Data::Int(i64::default()),
                &VariantDataType::Tristate => Data::Tristate(Tristate::default()),
            },
        }
    }

    pub fn convert_into(&self, vdt: &VariantDataType) -> Self {
        Self {
            data: match vdt {
                &VariantDataType::String => Data::String(self.clone().into()),
                &VariantDataType::Bool => Data::Bool(self.clone().into()),
                &VariantDataType::Hex => Data::Hex(self.clone().into()),
                &VariantDataType::Int => Data::Int(self.clone().into()),
                &VariantDataType::Tristate => Data::Tristate(self.clone().into()),
            },
        }
    }

    /// Converts the variant into a bool.
    ///
    /// If the variant contains a string value, the "n" or "N" string convert into
    /// a false boolean, other strings convert into a true boolean.
    ///
    /// If the variant contains a numeric value, either an integer or hexadecimal
    /// value, a 0 is converted into a false boolean, any other value into a true
    /// boolean.
    ///
    /// If the variant contains a tristate value, only the false tristate value
    /// will be converted into  a false boolean, either a true or maybe tristate
    /// value will be converted into a true value.
    ///
    /// If the variant contains a boolean value, it will be returned as is.
    pub fn to_bool(&self) -> bool {
        match &self.data {
            Data::String(s) => {
                if s == "y" || s == "Y" {
                    true
                } else {
                    false
                }
            }
            Data::Bool(b) => *b,
            Data::Tristate(t) => match t {
                &Tristate::FALSE => false,
                _ => true,
            },
            Data::Hex(h) => *h != Hex::from(0),
            Data::Int(i) => *i != 0,
        }
    }

    /// Converts the variant into a tristate.
    ///
    /// If the variant contains a string value, the "n" or "N" string convert into
    /// a false tristate, the "m" or "M" strings convert into a maybe tristate,
    /// any other strings convert into a true tristate.
    ///
    /// If the variant contains a numeric value, either an integer or hexadecimal
    /// value, a 0 is converted into a false tristate, any other value into a true
    /// tristate.
    ///
    /// If the variant contains a tristate value, it will be returned as is.
    ///
    /// If the variant contains a boolean value, it will be converted as tristate.
    pub fn to_tristate(&self) -> Tristate {
        match &self.data {
            Data::String(s) => {
                if s == "y" || s == "Y" {
                    Tristate::TRUE
                } else if s == "m" || s == "M" {
                    Tristate::MAYBE
                } else {
                    Tristate::FALSE
                }
            }
            Data::Bool(b) => Tristate::from(*b),
            Data::Tristate(t) => *t,
            Data::Hex(h) => {
                if *h != Hex::from(0) {
                    Tristate::TRUE
                } else {
                    Tristate::FALSE
                }
            }
            Data::Int(i) => {
                if *i != 0 {
                    Tristate::TRUE
                } else {
                    Tristate::FALSE
                }
            }
        }
    }

    /// Converts the variant into a hexadecimal value.
    ///
    /// If the variant contains a string value, an attempt will be made to convert
    /// the string in to an integer. If that fails, and the string contains a "n"
    /// or "N" value, it is converted into 0x00, otherwise it is interpreted as
    /// 0x01.
    ///
    /// If the variant contains a numeric value, either an integer or hexadecimal
    /// value, it will be returned as is, as hexadecimal representation. If the value
    /// is an integer, the sign will be converted.
    ///
    /// If the variant contains a tristate value, if the tristate is a false value,
    /// it will be converted as 0x00, otherwise it will be converted as 0x01.
    ///
    /// If the variant contains a boolean value, if the boolean is a false value,
    /// it will be converted as 0x00, otherwise it will be converted as 0x01.
    pub fn to_hex(&self) -> Hex {
        match &self.data {
            Data::String(s) => match Self::parse_hex(s) {
                Some(Data::Hex(h)) => h,
                _ => {
                    if s == "y" || s == "Y" {
                        Hex::from(1)
                    } else {
                        Hex::from(0)
                    }
                }
            },
            Data::Bool(b) => match b {
                true => Hex::from(1),
                false => Hex::from(0),
            },
            Data::Tristate(b) => match b {
                &Tristate::TRUE => Hex::from(1),
                &Tristate::FALSE => Hex::from(0),
                &Tristate::MAYBE => Hex::from(1),
            },
            Data::Hex(h) => *h,
            Data::Int(i) => Hex::from(*i as u64),
        }
    }

    /// Converts the variant into an integer value.
    ///
    /// If the variant contains a string value, an attempt will be made to convert
    /// the string in to an integer. If that fails, and the string contains a "n"
    /// or "N" value, it is converted into 0, otherwise it is interpreted as
    /// 1.
    ///
    /// If the variant contains a numeric value, either an integer or hexadecimal
    /// value, it will be returned as is, as hexadecimal representation. If the value
    /// is a hexadecimal, the sign will be converted.
    ///
    /// If the variant contains a tristate value, if the tristate is a false value,
    /// it will be converted as 0, otherwise it will be converted as 1.
    ///
    /// If the variant contains a boolean value, if the boolean is a false value,
    /// it will be converted as 0, otherwise it will be converted as 1.
    pub fn to_int(&self) -> i64 {
        match &self.data {
            Data::String(s) => match Self::parse_int(s) {
                Some(Data::Int(i)) => i,
                _ => {
                    if s == "y" || s == "Y" {
                        1i64
                    } else {
                        0i64
                    }
                }
            },
            Data::Bool(b) => match b {
                true => 1,
                false => 0,
            },
            Data::Tristate(b) => match b {
                &Tristate::TRUE => 1,
                &Tristate::FALSE => 0,
                &Tristate::MAYBE => 1,
            },
            Data::Hex(h) => {
                let value: u64 = (*h).into();
                value as i64
            }
            Data::Int(i) => *i,
        }
    }

    /// Converts the variant into an string.
    ///
    /// If the variant contains a string value, it is returned as is.
    ///
    /// If the variant contains a numeric value, either an integer or hexadecimal
    /// value, it will be converted into a string, either as numeric rendering or
    /// hexadecimal rendering.
    ///
    /// If the variant contains a tristate value, if the tristate is a false value,
    /// it will be converted as "n", if it is a maybe value, it will be converted
    /// as "m", otherwise it will be converted as "y".
    ///
    /// If the variant contains a boolean value, if the boolean is a false value,
    /// it will be converted as "n", otherwise it will be converted as "y".
    fn to_string(&self) -> String {
        match &self.data {
            Data::String(s) => s.clone(),
            Data::Bool(b) => match b {
                &true => "y".to_owned(),
                &false => "n".to_owned(),
            },
            Data::Tristate(t) => match t {
                &Tristate::TRUE => "y".to_owned(),
                &Tristate::FALSE => "n".to_owned(),
                &Tristate::MAYBE => "m".to_owned(),
            },
            Data::Hex(h) => h.to_string(),
            Data::Int(i) => i.to_string(),
        }
    }

    fn parse_hex(v: &str) -> Option<Data> {
        let v = v.replace("_", "").replace(" ", "");
        let mut chars = v.chars();

        let mut result = 0u64;

        if let Some(next_char) = chars.next() {
            if next_char == '0' {
                // This possibly is a hexadecimal character
                if let Some(next_char) = chars.next() {
                    if next_char == 'x' || next_char == 'X' {
                        // More likely a hexadecimal character
                        let mut repeat = true;

                        while repeat {
                            repeat = if let Some(c) = chars.next() {
                                result <<= 4u64;
                                match c.to_digit(16) {
                                    Some(v) => result |= v as u64,
                                    None => return None,
                                }
                                true
                            } else {
                                false
                            }
                        }

                        return Some(Data::Hex(Hex::from(result)));
                    }
                }
            }
        }

        None
    }

    fn parse_int(v: &str) -> Option<Data> {
        let v = v.replace("_", "").replace(" ", "");
        let chars = v.chars();

        let mut result = 0i64;

        let mut is_negative = false;
        let mut is_first = true;

        for c in chars {
            if is_first && c == '-' {
                is_negative = true;
                continue;
            }
            is_first = false;
            result *= 10;
            match c.to_digit(10) {
                Some(v) => result += v as i64,
                None => return None,
            }
        }

        if is_negative {
            result = 0 - result;
        }

        return Some(Data::Int(result));
    }

    /// Parses a given string, and attempts to return the closest matching
    /// variant belonging to the string. If the string contains a "y" or "Y"
    /// value, it is converted to a boolean true value, if the string contains
    /// a "n" or "N" value, it is converted to a boolean false value, if the
    /// string contains a "m" or "M" value, it is converted into a tristate
    /// maybe value.
    ///
    /// If none of the above are true, an attempt is made to parse the string
    /// as hexadecimal and integer values. If unsuccessful, it is considered
    /// a normal string.
    pub fn parse(v: &str) -> Self {
        // If v == "y" or v == "Y" the current value is a boolean true
        let data = if v == "y" || v == "Y" {
            Data::Bool(true)
        }
        // If v == "n" or v == "N" the current value is a boolean false
        else if v == "n" || v == "N" {
            Data::Bool(false)
        // If v == "m" or v == "M" the current value is a tristate maybe
        } else if v == "m" || v == "M" {
            Data::Tristate(Tristate::MAYBE)
        } else {
            // Attempt to convert the value to an integer (when possible)
            if let Some(result) = Self::parse_int(v) {
                result
            // Attempt to convert the value to a hexadecimal integer (when possible)
            } else if let Some(result) = Self::parse_hex(v) {
                result
            // Fall back to a string
            } else {
                Data::String(v.to_string())
            }
        };

        Self { data: data }
    }

    /// Converts a dot config value back into a Variant
    pub fn from_dot_config(v: &str) -> Self {
        match parse_string(v) {
            Ok(s) => Self {
                data: Data::String(s),
            },
            Err(_) => Self::parse(v),
        }
    }

    /// Converts the representation of the variant from a normal value to a .config
    /// representation, as used by the Kconfig intermediate format.
    pub fn dot_config(&self) -> String {
        match &self.data {
            Data::Bool(b) => match b {
                true => "y".to_owned(),
                false => "n".to_owned(),
            },
            Data::Tristate(t) => match t {
                &Tristate::TRUE => "y".to_owned(),
                &Tristate::MAYBE => "m".to_owned(),
                &Tristate::FALSE => "n".to_owned(),
            },
            Data::String(s) => escape_string(&s),
            Data::Int(i) => i.to_string(),
            Data::Hex(h) => h.to_string(),
        }
    }

    /// Transforms the variant from the current variant type, into the given variant type
    pub fn transform(&self, vdt: VariantDataType) -> Variant {
        match vdt {
            VariantDataType::Bool => Variant::from(self.to_bool()),
            VariantDataType::Hex => Variant::from(self.to_hex()),
            VariantDataType::Int => Variant::from(self.to_int()),
            VariantDataType::Tristate => Variant::from(self.to_tristate()),
            VariantDataType::String => Variant::from(self.to_string()),
        }
    }
}

impl Display for Variant {
    /// Formats the display of the variant as value
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        f.write_str(&self.to_string())
    }
}

impl From<&str> for Variant {
    /// Creates a variant from a string
    fn from(v: &str) -> Self {
        Self {
            data: Data::String(v.to_string()),
        }
    }
}

impl From<String> for Variant {
    /// Creates a variant from a string
    fn from(v: String) -> Self {
        Self {
            data: Data::String(v),
        }
    }
}

impl From<bool> for Variant {
    /// Creates a variant from a boolean value
    fn from(b: bool) -> Self {
        Self {
            data: Data::Bool(b),
        }
    }
}

impl From<Tristate> for Variant {
    /// Creates a variant from a tristate value
    fn from(t: Tristate) -> Self {
        Self {
            data: Data::Tristate(t),
        }
    }
}

impl From<i64> for Variant {
    /// Creates a variant from an integer value
    fn from(i: i64) -> Self {
        Self { data: Data::Int(i) }
    }
}

impl From<Hex> for Variant {
    /// Creates a variant form a hexadecimal value
    fn from(h: Hex) -> Self {
        Self { data: Data::Hex(h) }
    }
}

impl Not for Variant {
    type Output = Variant;

    /// Inverts the variant. If the variant is a boolean,
    /// inverts it directly. If the variant is not a boolean,
    /// it is converted to a tristate value, then it is inverted,
    /// such that the maybe tristate value, retains its properties.
    fn not(self) -> Self::Output {
        if let Data::Bool(v) = &self.data {
            Variant {
                data: Data::Bool(!v),
            }
        } else {
            let v = self.to_tristate();
            Variant {
                data: Data::Tristate(!v),
            }
        }
    }
}

impl Into<bool> for Variant {
    /// Converts the variant into a boolean value, using the
    /// [`Self::to_bool()`] function.
    fn into(self) -> bool {
        self.to_bool()
    }
}

impl Into<String> for Variant {
    /// Converts the variant into a string, using the
    /// [`Self::to_string()`] function.
    fn into(self) -> String {
        self.to_string()
    }
}

impl Into<Tristate> for Variant {
    /// Converts the variant into a tristate value, using the
    /// [`Self::to_tristate()`] function.
    fn into(self) -> Tristate {
        self.to_tristate()
    }
}

impl Into<i64> for Variant {
    /// Converts the variant into an integer value, using the
    /// [`Self::to_int()`] function.
    fn into(self) -> i64 {
        self.to_int()
    }
}

impl Into<Hex> for Variant {
    /// Converts the variant into a hexadecimal value, using the
    /// [`Self::to_hex()`] function.
    fn into(self) -> Hex {
        self.to_hex()
    }
}

impl Ord for Variant {
    /// If self and other are both of integer or hexadecimal type,
    /// then comparison will be performed by integer comparison.
    ///
    /// If self and other are both of boolean or tristate type,
    /// then comparison will be performed on tristate value,
    /// thus y > m > n.
    ///
    /// If one is of type boolean/tristate and the other is of
    /// type integer/hexadecimal, the integer/hexadecimal type
    /// will be converted to it's boolean equivalent, then
    /// compared
    ///
    /// Everything else will be compared as if it were two
    /// string types.
    fn cmp(&self, other: &Self) -> Ordering {
        if self.datatype().numberlike() && other.datatype().numberlike() {
            let me: i64 = self.clone().into();
            let other: i64 = other.clone().into();
            return me.cmp(&other);
        }

        if self.datatype().boollike() && other.datatype().boollike() {
            let me: Tristate = self.clone().into();
            let other: Tristate = other.clone().into();
            return me.cmp(&other);
        }

        if self.datatype().numberlike() && other.datatype().boollike()
            || self.datatype().boollike() && other.datatype().numberlike()
        {
            let me: Tristate = self.clone().into();
            let other: Tristate = other.clone().into();
            return me.cmp(&other);
        }

        let me: String = self.clone().into();
        let other: String = self.clone().into();
        me.cmp(&other)
    }
}

impl PartialOrd for Variant {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(PartialEq, Eq, Clone, Debug)]
enum Data {
    String(String),
    Bool(bool),
    Tristate(Tristate),
    Hex(Hex),
    Int(i64),
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Debug)]
pub enum VariantDataType {
    /// The data type underpinning the variant is a character string
    String,
    /// The data type underpinning the variant is a boolean value
    Bool,
    /// The data type underpinning the variant is a tristate value
    Tristate,
    /// The data type underpinning the variant is a hexadecimal value
    Hex,
    /// The data type underpinning the variant is an integer value
    Int,
}

impl VariantDataType {
    pub(self) fn numberlike(&self) -> bool {
        match self {
            Self::Hex | Self::Int => true,
            _ => false,
        }
    }

    pub(self) fn boollike(&self) -> bool {
        match self {
            Self::Bool | Self::Tristate => true,
            _ => false,
        }
    }
}

impl From<&ConfigType> for VariantDataType {
    /// Converts a given ConfigType value to the associated VariantDataType value
    fn from(ct: &ConfigType) -> Self {
        match ct {
            ConfigType::Bool => Self::Bool,
            ConfigType::String => Self::String,
            ConfigType::Tristate => Self::Tristate,
            ConfigType::Hex => Self::Hex,
            ConfigType::Int => Self::Int,
        }
    }
}

impl Into<ConfigType> for VariantDataType {
    /// Converts a given VariantDataType back into a ConfigType
    fn into(self) -> ConfigType {
        match self {
            Self::Bool => ConfigType::Bool,
            Self::String => ConfigType::String,
            Self::Tristate => ConfigType::Tristate,
            Self::Hex => ConfigType::Hex,
            Self::Int => ConfigType::Int,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::Hex;
    use crate::Tristate;
    use crate::{Error, Variant, VariantDataType};
    use kconfig_parser::ast::ConfigType;

    #[test]
    fn bool_variant_false() -> Result<(), Error> {
        let result: bool = Variant::from(false).into();
        assert_eq!(false, result);
        let result: String = Variant::from(false).into();
        assert_eq!("n".to_owned(), result);
        let result: Tristate = Variant::from(false).into();
        assert_eq!(Tristate::FALSE, result);
        let result: i64 = Variant::from(false).into();
        assert_eq!(0, result);
        let result: Hex = Variant::from(false).into();
        assert_eq!(Hex::from(0), result);
        Ok(())
    }

    #[test]
    fn bool_variant_true() -> Result<(), Error> {
        let result: bool = Variant::from(true).into();
        assert_eq!(true, result);
        let result: String = Variant::from(true).into();
        assert_eq!("y".to_owned(), result);
        let result: Tristate = Variant::from(true).into();
        assert_eq!(Tristate::TRUE, result);
        let result: i64 = Variant::from(true).into();
        assert_eq!(1, result);
        let result: Hex = Variant::from(true).into();
        assert_eq!(Hex::from(1), result);
        Ok(())
    }

    #[test]
    fn string_variant_falsely() -> Result<(), Error> {
        let result: bool = Variant::from("n").into();
        assert_eq!(false, result);
        let result: String = Variant::from("n").into();
        assert_eq!("n".to_owned(), result);
        let result: Tristate = Variant::from("n").into();
        assert_eq!(Tristate::FALSE, result);
        let result: i64 = Variant::from("n").into();
        assert_eq!(0, result);
        let result: Hex = Variant::from("n").into();
        assert_eq!(Hex::from(0), result);
        Ok(())
    }

    #[test]
    fn string_variant_maybe() -> Result<(), Error> {
        let result: Tristate = Variant::from("m").into();
        assert_eq!(Tristate::MAYBE, result);
        Ok(())
    }

    #[test]
    fn string_variant_truth() -> Result<(), Error> {
        let result: bool = Variant::from("y").into();
        assert_eq!(true, result);
        let result: String = Variant::from("y").into();
        assert_eq!("y".to_owned(), result);
        let result: Tristate = Variant::from("y").into();
        assert_eq!(Tristate::TRUE, result);
        let result: i64 = Variant::from("y").into();
        assert_eq!(1, result);
        let result: Hex = Variant::from("y").into();
        assert_eq!(Hex::from(1), result);

        let result: bool = Variant::from("foo").into();
        assert_eq!(false, result);
        let result: String = Variant::from("foo").into();
        assert_eq!("foo".to_owned(), result);
        let result: Tristate = Variant::from("foo").into();
        assert_eq!(Tristate::FALSE, result);
        let result: i64 = Variant::from("foo").into();
        assert_eq!(0, result);
        let result: Hex = Variant::from("foo").into();
        assert_eq!(Hex::from(0), result);
        Ok(())
    }

    #[test]
    fn tristate_variant_falsely() -> Result<(), Error> {
        let result: bool = Variant::from(Tristate::FALSE).into();
        assert_eq!(false, result);
        let result: String = Variant::from(Tristate::FALSE).into();
        assert_eq!("n".to_owned(), result);
        let result: Tristate = Variant::from(Tristate::FALSE).into();
        assert_eq!(Tristate::FALSE, result);
        let result: i64 = Variant::from(Tristate::FALSE).into();
        assert_eq!(0, result);
        let result: Hex = Variant::from(Tristate::FALSE).into();
        assert_eq!(Hex::from(0), result);
        Ok(())
    }

    #[test]
    fn tristate_variant_true() -> Result<(), Error> {
        let result: bool = Variant::from(Tristate::TRUE).into();
        assert_eq!(true, result);
        let result: String = Variant::from(Tristate::TRUE).into();
        assert_eq!("y".to_owned(), result);
        let result: Tristate = Variant::from(Tristate::TRUE).into();
        assert_eq!(Tristate::TRUE, result);
        let result: i64 = Variant::from(Tristate::TRUE).into();
        assert_eq!(1, result);
        let result: Hex = Variant::from(Tristate::TRUE).into();
        assert_eq!(Hex::from(1), result);

        let result: bool = Variant::from(Tristate::MAYBE).into();
        assert_eq!(true, result);
        let result: String = Variant::from(Tristate::MAYBE).into();
        assert_eq!("m".to_owned(), result);
        let result: Tristate = Variant::from(Tristate::MAYBE).into();
        assert_eq!(Tristate::MAYBE, result);
        let result: i64 = Variant::from(Tristate::MAYBE).into();
        assert_eq!(1, result);
        let result: Hex = Variant::from(Tristate::MAYBE).into();
        assert_eq!(Hex::from(1), result);
        Ok(())
    }

    #[test]
    fn int_variant_falsely() -> Result<(), Error> {
        let result: bool = Variant::from(0).into();
        assert_eq!(false, result);
        let result: String = Variant::from(0).into();
        assert_eq!("0".to_owned(), result);
        let result: Tristate = Variant::from(0).into();
        assert_eq!(Tristate::FALSE, result);
        let result: i64 = Variant::from(0).into();
        assert_eq!(0, result);
        let result: Hex = Variant::from(0).into();
        assert_eq!(Hex::from(0), result);
        Ok(())
    }

    #[test]
    fn int_variant_truth() -> Result<(), Error> {
        for number in vec![1, 2, -1] {
            let result: bool = Variant::from(number).into();
            assert_eq!(true, result);
            let result: String = Variant::from(number).into();
            let s = number.to_string();
            assert_eq!(s, result);
            let result: Tristate = Variant::from(number).into();
            assert_eq!(Tristate::TRUE, result);
            let result: i64 = Variant::from(number).into();
            assert_eq!(number, result);
            let result: Hex = Variant::from(number).into();
            if number > 0 {
                assert_eq!(Hex::from(number as u64), result);
            } else if number == -1 {
                assert_eq!(Hex::from(0xffffffffffffffff), result);
            }
        }
        Ok(())
    }

    #[test]
    fn hex_variant_falsely() -> Result<(), Error> {
        let number = Hex::from(0);
        let result: bool = Variant::from(number).into();
        assert_eq!(false, result);
        let result: String = Variant::from(number).into();
        assert_eq!("0x0".to_owned(), result);
        let result: Tristate = Variant::from(number).into();
        assert_eq!(Tristate::FALSE, result);
        let result: i64 = Variant::from(number).into();
        assert_eq!(0, result);
        let result: Hex = Variant::from(number).into();
        assert_eq!(Hex::from(0), result);
        Ok(())
    }

    #[test]
    fn hex_variant_truth() -> Result<(), Error> {
        for number in vec![Hex::from(1), Hex::from(2), Hex::from(0xffffffffffffffff)] {
            let result: bool = Variant::from(number).into();
            assert_eq!(true, result);
            let result: String = Variant::from(number).into();
            let s = number.to_string();
            assert_eq!(s, result);
            let result: Tristate = Variant::from(number).into();
            assert_eq!(Tristate::TRUE, result);
            let result: i64 = Variant::from(number).into();
            if number <= Hex::from(2) {
                let value: u64 = number.into();
                assert_eq!(value as i64, result);
            } else if number == Hex::from(0xffffffffffffffff) {
                assert_eq!(-1, result);
            }
            let result: Hex = Variant::from(number).into();
            assert_eq!(number, result);
        }
        Ok(())
    }

    #[test]
    fn datatype() -> Result<(), Error> {
        assert_eq!(VariantDataType::Bool, Variant::from(false).datatype());
        assert_eq!(VariantDataType::Bool, Variant::from(true).datatype());
        assert_eq!(
            VariantDataType::Tristate,
            Variant::from(Tristate::FALSE).datatype()
        );
        assert_eq!(
            VariantDataType::Tristate,
            Variant::from(Tristate::TRUE).datatype()
        );
        assert_eq!(
            VariantDataType::Tristate,
            Variant::from(Tristate::MAYBE).datatype()
        );
        for number in vec![0, 1, 2, -1] {
            assert_eq!(VariantDataType::Int, Variant::from(number).datatype());
        }
        for number in vec![Hex::from(1), Hex::from(2), Hex::from(0xffffffffffffffff)] {
            assert_eq!(VariantDataType::Hex, Variant::from(number).datatype());
        }
        assert_eq!(VariantDataType::String, Variant::from("").datatype());
        Ok(())
    }

    #[test]
    fn new_types() -> Result<(), Error> {
        assert_eq!(
            "".to_owned(),
            Variant::new_typed(&VariantDataType::String).to_string()
        );
        assert_eq!(false, Variant::new_typed(&VariantDataType::Bool).to_bool());
        assert_eq!(
            Tristate::FALSE,
            Variant::new_typed(&VariantDataType::Tristate).to_tristate()
        );
        assert_eq!(0, Variant::new_typed(&VariantDataType::Int).to_int());
        assert_eq!(
            Hex::from(0),
            Variant::new_typed(&VariantDataType::Hex).to_hex()
        );
        Ok(())
    }

    #[test]
    fn to_hex() -> Result<(), Error> {
        let value = Variant::from("123456789");
        let result: i64 = value.into();
        assert_eq!(123456789, result);

        // Negative
        let value = Variant::from("-123456789");
        let result: i64 = value.into();
        assert_eq!(-123456789, result);

        // Allow spaces
        let value = Variant::from(" 123 456 789 ");
        let result: i64 = value.into();
        assert_eq!(123456789, result);

        // Allow underscores
        let value = Variant::from("_123_456_789_");
        let result: i64 = value.into();
        assert_eq!(123456789, result);

        let value = Variant::from("0x123456789abcdef");
        let result: Hex = value.into();
        assert_eq!(Hex::from(0x123456789abcdef), result);

        // Allow spaces
        let value = Variant::from(" 0 x 1234 5678 9abc def0 ");
        let result: Hex = value.into();
        assert_eq!(Hex::from(0x123456789abcdef0), result);

        // Allow underscores
        let value = Variant::from("_0_x_1234_5678_9abc_def0_");
        let result: Hex = value.into();
        assert_eq!(Hex::from(0x123456789abcdef0), result);

        Ok(())
    }

    #[test]
    fn not() -> Result<(), Error> {
        let value = Variant::from(true);
        let inverse = !value;
        assert_eq!(Variant::from(false), inverse);

        let value = Variant::from(false);
        let inverse = !value;
        assert_eq!(Variant::from(true), inverse);

        let value = Variant::from(Tristate::TRUE);
        let inverse = !value;
        assert_eq!(Variant::from(Tristate::FALSE), inverse);

        let value = Variant::from(Tristate::MAYBE);
        let inverse = !value;
        assert_eq!(Variant::from(Tristate::MAYBE), inverse);

        let value = Variant::from(Tristate::FALSE);
        let inverse = !value;
        assert_eq!(Variant::from(Tristate::TRUE), inverse);

        Ok(())
    }

    #[test]
    fn datatype_2_config_type() -> Result<(), Error> {
        let config_type: ConfigType = VariantDataType::Bool.into();
        assert_eq!(ConfigType::Bool, config_type);

        let config_type: ConfigType = VariantDataType::Int.into();
        assert_eq!(ConfigType::Int, config_type);

        let config_type: ConfigType = VariantDataType::String.into();
        assert_eq!(ConfigType::String, config_type);

        let config_type: ConfigType = VariantDataType::Hex.into();
        assert_eq!(ConfigType::Hex, config_type);

        let config_type: ConfigType = VariantDataType::Tristate.into();
        assert_eq!(ConfigType::Tristate, config_type);

        Ok(())
    }

    #[test]
    fn config_type_2_datatype() -> Result<(), Error> {
        let datatype = VariantDataType::from(&ConfigType::Bool);
        assert_eq!(VariantDataType::Bool, datatype);

        let datatype = VariantDataType::from(&ConfigType::Tristate);
        assert_eq!(VariantDataType::Tristate, datatype);

        let datatype = VariantDataType::from(&ConfigType::Hex);
        assert_eq!(VariantDataType::Hex, datatype);

        let datatype = VariantDataType::from(&ConfigType::String);
        assert_eq!(VariantDataType::String, datatype);

        let datatype = VariantDataType::from(&ConfigType::Int);
        assert_eq!(VariantDataType::Int, datatype);

        Ok(())
    }
}
