/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//! This file implements errors returned from the kconfig representation

use std::{fmt::Display, io};

/// An Error is returned when the representation of the AST into a higher-level
/// presentation failed.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Error {
    msg: String,
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.msg,)
    }
}

impl From<String> for Error {
    fn from(s: String) -> Self {
        Error::new(&s)
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Self {
            msg: format!("{}", e),
        }
    }
}

impl From<&str> for Error {
    fn from(s: &str) -> Self {
        Error::new(s)
    }
}

impl From<kconfig_parser::Error> for Error {
    fn from(e: kconfig_parser::Error) -> Self {
        Error::new(&format!("{}", e))
    }
}

impl From<Vec<(String, String)>> for Error {
    fn from(invalid_lines: Vec<(String, String)>) -> Self {
        let mut err_msg = String::new();
        for (msg, line) in invalid_lines {
            err_msg.push_str(&format!("{}: {}\n", msg, line));
        }
        Error::new(&err_msg)
    }
}

impl Error {
    pub(crate) fn new(msg: &str) -> Self {
        Self {
            msg: msg.to_string(),
        }
    }
}

/// A LoadError is returned when during loading of a .config file, errors where found.
/// Typically, this is non-destructive, but an end-user should be warned through the
/// frontend that something went wrong during loading of the .config file.
pub enum LoadError {
    IoError(io::Error),
    Error(Error),
}

impl From<io::Error> for LoadError {
    fn from(e: io::Error) -> Self {
        LoadError::IoError(e)
    }
}

impl From<Error> for LoadError {
    fn from(e: Error) -> Self {
        LoadError::Error(e)
    }
}

impl Display for LoadError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LoadError::IoError(e) => write!(f, "{}", e),
            LoadError::Error(e) => write!(f, "{}", e),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::io;

    use crate::{Error, LoadError};

    #[test]
    fn ctor() -> Result<(), Error> {
        let e = Error {
            msg: "FOO".to_owned(),
        };

        assert_eq!("FOO".to_owned(), format!("{e}"));

        Ok(())
    }

    #[test]
    fn new() -> Result<(), Error> {
        let e = Error::new("FOO");
        assert_eq!("FOO".to_owned(), format!("{e}"));
        Ok(())
    }

    #[test]
    fn from_string() -> Result<(), Error> {
        let e = Error::from("FOO".to_owned());
        assert_eq!("FOO".to_owned(), format!("{e}"));
        Ok(())
    }

    #[test]
    fn from_str() -> Result<(), Error> {
        let e = Error::from("FOO");
        assert_eq!("FOO".to_owned(), format!("{e}"));
        Ok(())
    }

    #[test]
    fn from_io_error() -> Result<(), Error> {
        let ioe = io::Error::new(io::ErrorKind::Unsupported, "FOO");
        let e = Error::from(ioe);
        assert_eq!("FOO".to_owned(), format!("{e}"));
        Ok(())
    }

    #[test]
    fn from_lines() -> Result<(), Error> {
        let invalid_lines = vec![
            ("FOO".to_owned(), "1".to_owned()),
            ("BAR".to_owned(), "2".to_owned()),
        ];
        let e = Error::from(invalid_lines);
        assert_eq!("FOO: 1\nBAR: 2\n", format!("{e}"));
        Ok(())
    }

    #[test]
    fn load_error_from_io_error() -> Result<(), Error> {
        let ioe = io::Error::new(io::ErrorKind::Unsupported, "FOO");
        let le = LoadError::from(ioe);
        assert_eq!("FOO", format!("{le}"));
        Ok(())
    }

    #[test]
    fn load_error_from_error() -> Result<(), Error> {
        let e = Error::from("FOO");
        let le = LoadError::from(e);
        assert_eq!("FOO", format!("{le}"));
        Ok(())
    }
}
