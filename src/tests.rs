use super::{ConfigRegistry, ItemReference, MutationStart, Tristate, Variant, VariantDataType};
use crate::error::Error;
use crate::Hex;
use kconfig_parser::Ast;
use kconfig_parser::Lexer;
use kconfig_parser::MacroLexer;

fn registry_from(s: &str) -> Result<ConfigRegistry, Error> {
    let mut lexer = MacroLexer::new(Lexer::create(s.as_bytes()), &());
    let ast = Ast::parse(&mut lexer)?;
    ConfigRegistry::new(&ast, &lexer.symbol_table())
}

trait WriteLoadTest {
    fn writebuf(&self) -> Result<Vec<u8>, Error>;
    fn loadbuf(&mut self, buffer: &Vec<u8>) -> Result<(), Error>;
}

impl WriteLoadTest for ConfigRegistry {
    fn writebuf(&self) -> Result<Vec<u8>, Error> {
        let mut buffer: Vec<u8> = Vec::new();
        self.write_dotconfig(&mut buffer)?;
        Ok(buffer)
    }

    fn loadbuf(&mut self, buffer: &Vec<u8>) -> Result<(), Error> {
        //eprintln!("Buffer: {:?}", String::from_utf8(buffer.clone()));
        match self.read_dotconfig(&mut buffer.as_slice()) {
            Ok(_) => Ok(()),
            Err(e) => Err(Error::from(format!("{}", e))),
        }
    }
}

#[test]
fn test_mainmenu() -> Result<(), Error> {
    let registry = registry_from(&"mainmenu \"Hello World\"")?;
    assert_eq!(registry.main_menu_name(), "Hello World".to_string());
    let menu = registry.main_menu_item();
    assert_eq!(menu.name(), "Hello World");
    Ok(())
}

#[test]
fn test_no_mainmenu() -> Result<(), Error> {
    let registry = registry_from(&"")?;
    assert_eq!(registry.main_menu_name(), "ROOT".to_string());
    let menu = registry.main_menu_item();
    assert_eq!(menu.name(), "ROOT");
    Ok(())
}

#[test]
fn test_basic_config() -> Result<(), Error> {
    let input = concat!(
        "config STRING\n   string   \"string\"\n   default \"string\"\n",
        "config INT\n      int      \"int\"\n      default 1\n",
        "config HEX\n      hex      \"hex\"\n      default 0x1\n",
        "config BOOL\n     bool     \"bool\"\n     default y\n",
        "config TRISTATE\n tristate \"tristate\"\n default m\n",
    );

    let mut registry = registry_from(&input)?;

    let string_ref = ItemReference::Config("STRING".to_owned());
    let int_ref = ItemReference::Config("INT".to_owned());
    let hex_ref = ItemReference::Config("HEX".to_owned());
    let bool_ref = ItemReference::Config("BOOL".to_owned());
    let tristate_ref = ItemReference::Config("TRISTATE".to_owned());

    for itemref in vec![string_ref, int_ref, hex_ref, bool_ref, tristate_ref] {
        let item = registry.get(&itemref).unwrap();
        assert_eq!(item.name(), itemref.to_string());
        assert_eq!(item.itemref(), itemref);
        assert_eq!(item.config_items().len(), 1);
        assert_eq!(true, item.info().is_empty());

        let (_, config_item) = item.config_items().iter().next().unwrap();
        assert_eq!(
            config_item.clone(),
            registry.current_config_item(&itemref).unwrap()
        );
        assert_eq!(
            config_item.prompt().clone().unwrap(),
            itemref.to_string().to_lowercase()
        );

        assert_eq!(config_item.defaults().len(), 1);
        assert_eq!(config_item.defaults().iter().next().unwrap().1, &None);
        assert_eq!(item.item_dependencies().len(), 1);
        assert_eq!(item.selected().len(), 0);
        assert_eq!(item.implied().len(), 0);
        assert_eq!(item.selects_me().len(), 0);
        assert_eq!(item.imply_me().len(), 0);
        assert_eq!(true, registry.current_value_is_default(&itemref));
        assert_eq!(true, registry.item_enabled(&itemref).unwrap());

        let outbuf = registry.writebuf()?;
        registry.loadbuf(&outbuf)?;

        assert_eq!(true, registry.current_value_is_default(&itemref));
    }

    let string_ref = ItemReference::Config("STRING".to_owned());
    let int_ref = ItemReference::Config("INT".to_owned());
    let hex_ref = ItemReference::Config("HEX".to_owned());
    let bool_ref = ItemReference::Config("BOOL".to_owned());
    let tristate_ref = ItemReference::Config("TRISTATE".to_owned());

    assert_eq!(
        MutationStart::String("string".to_owned()),
        registry.mutation_start(&string_ref).unwrap()
    );

    assert_eq!(
        MutationStart::Int(1),
        registry.mutation_start(&int_ref).unwrap()
    );

    assert_eq!(
        MutationStart::Hex(Hex::from(0x1)),
        registry.mutation_start(&hex_ref).unwrap()
    );

    assert_eq!(
        MutationStart::Bool(true),
        registry.mutation_start(&bool_ref).unwrap()
    );

    assert_eq!(
        MutationStart::Tristate(Tristate::MAYBE),
        registry.mutation_start(&tristate_ref).unwrap()
    );

    assert_eq!(
        "\"string\"".to_owned(),
        registry.current_value(&string_ref).unwrap().dot_config(),
    );

    assert_eq!(
        "1".to_owned(),
        registry.current_value(&int_ref).unwrap().dot_config(),
    );

    assert_eq!(
        "0x1".to_owned(),
        registry.current_value(&hex_ref).unwrap().dot_config(),
    );

    assert_eq!(
        "y".to_owned(),
        registry.current_value(&bool_ref).unwrap().dot_config(),
    );

    assert_eq!(
        "m".to_owned(),
        registry.current_value(&tristate_ref).unwrap().dot_config(),
    );

    let itemref = ItemReference::Menu("ROOT".to_owned());
    let item = registry.get(&itemref).unwrap();
    assert_eq!(item.name(), itemref.to_string());
    assert_eq!(item.itemref(), itemref);
    assert_eq!(item.config_items().len(), 0);
    assert_eq!(item.item_dependencies().len(), 0);
    assert_eq!(item.selected().len(), 0);
    assert_eq!(item.implied().len(), 0);
    assert_eq!(item.selects_me().len(), 0);
    assert_eq!(item.imply_me().len(), 0);

    assert_eq!(None, registry.mutation_start(&itemref));
    assert_eq!(None, registry.current_value(&itemref));

    Ok(())
}

#[test]
fn test_empty_config() -> Result<(), Error> {
    let input = concat!(
        "config STRING\n string \"string\"\n",
        "config INT\n int \"int\"\n",
        "config HEX\n hex \"hex\"\n",
        "config BOOL\n bool \"bool\"\n",
        "config TRISTATE\n tristate \"tristate\"\n",
    );

    let mut registry = registry_from(&input)?;

    let string_ref = ItemReference::Config("STRING".to_owned());
    let int_ref = ItemReference::Config("INT".to_owned());
    let hex_ref = ItemReference::Config("HEX".to_owned());
    let bool_ref = ItemReference::Config("BOOL".to_owned());
    let tristate_ref = ItemReference::Config("TRISTATE".to_owned());

    for itemref in vec![string_ref, int_ref, hex_ref, bool_ref, tristate_ref] {
        let item = registry.get(&itemref).unwrap();
        assert_eq!(item.name(), itemref.to_string());
        assert_eq!(item.config_items().len(), 1);
        assert_eq!(true, item.info().is_empty());

        let (_, config_item) = item.config_items().iter().next().unwrap();
        assert_eq!(
            config_item.clone(),
            registry.current_config_item(&itemref).unwrap()
        );
        assert_eq!(
            config_item.prompt().clone().unwrap(),
            itemref.to_string().to_lowercase()
        );
        assert_eq!(config_item.defaults().len(), 0);
        assert_eq!(item.item_dependencies().len(), 1);
        assert_eq!(item.selected().len(), 0);
        assert_eq!(item.implied().len(), 0);
        assert_eq!(item.selects_me().len(), 0);
        assert_eq!(item.imply_me().len(), 0);

        assert_eq!(true, registry.current_value_is_default(&itemref));

        assert_eq!(true, registry.item_enabled(&itemref).unwrap());

        let outbuf = registry.writebuf()?;
        registry.loadbuf(&outbuf)?;

        assert_eq!(true, registry.current_value_is_default(&itemref));
    }

    let string_ref = ItemReference::Config("STRING".to_owned());
    let int_ref = ItemReference::Config("INT".to_owned());
    let hex_ref = ItemReference::Config("HEX".to_owned());
    let bool_ref = ItemReference::Config("BOOL".to_owned());
    let tristate_ref = ItemReference::Config("TRISTATE".to_owned());

    assert_eq!(
        MutationStart::String("".to_owned()),
        registry.mutation_start(&string_ref).unwrap()
    );

    assert_eq!(
        MutationStart::Int(0),
        registry.mutation_start(&int_ref).unwrap()
    );

    assert_eq!(
        MutationStart::Hex(Hex::from(0)),
        registry.mutation_start(&hex_ref).unwrap()
    );

    assert_eq!(
        MutationStart::Bool(false),
        registry.mutation_start(&bool_ref).unwrap()
    );

    assert_eq!(
        MutationStart::Tristate(Tristate::FALSE),
        registry.mutation_start(&tristate_ref).unwrap()
    );

    assert_eq!(
        "\"\"".to_owned(),
        registry.current_value(&string_ref).unwrap().dot_config(),
    );

    assert_eq!(
        "0".to_owned(),
        registry.current_value(&int_ref).unwrap().dot_config(),
    );

    assert_eq!(
        "0x0".to_owned(),
        registry.current_value(&hex_ref).unwrap().dot_config(),
    );

    assert_eq!(
        "n".to_owned(),
        registry.current_value(&bool_ref).unwrap().dot_config(),
    );

    assert_eq!(
        "n".to_owned(),
        registry.current_value(&tristate_ref).unwrap().dot_config(),
    );

    Ok(())
}

#[test]
fn test_disabled_string_config() -> Result<(), Error> {
    let registry = registry_from(&"if BAR\n     config FOO\n    string \"foo\"\n     endif")?;
    let itemref = ItemReference::Config("FOO".to_owned());
    assert_eq!(false, registry.item_enabled(&itemref)?);
    let item = registry.get(&itemref).unwrap();
    assert_eq!(1, item.config_items().len());
    for (_, config_item) in item.config_items() {
        assert_eq!(Variant::from(false), config_item.choice_low());
        assert_eq!(false, config_item.is_menu());
    }
    Ok(())
}

#[test]
fn test_string_config_with_help() -> Result<(), Error> {
    let registry =
        registry_from(&"config FOO\n    string \"foo\"\n    default bar\n    help\n        Help")?;
    let itemref = ItemReference::Config("FOO".to_owned());
    let info = registry.get_info(&itemref).unwrap();
    assert_eq!(false, info.is_empty());
    let v: Vec<String> = info.iter().collect();
    assert_eq!(v, vec!("Help".to_owned()));
    Ok(())
}

#[test]
fn test_string_config_from_string_value() -> Result<(), Error> {
    let mut registry = registry_from(&"config FOO\n    string \"foo\"")?;
    let itemref = ItemReference::Config("FOO".to_owned());
    registry.set_value(&itemref, Variant::from("bar"))?;
    let out = String::from_utf8(registry.writebuf()?).unwrap();
    assert_eq!(true, out.contains("FOO=\"bar\""));
    Ok(())
}

#[test]
fn test_from_dot_config() -> Result<(), Error> {
    let input = concat!(
        "config STRING\n   string   \"string\"\n   default \"string\"\n",
        "config INT\n      int      \"int\"\n      default 1\n",
        "config HEX\n      hex      \"hex\"\n      default 0x1\n",
        "config BOOL\n     bool     \"bool\"\n     default y\n",
        "config TRISTATE\n tristate \"tristate\"\n default m\n",
    );

    let dotconfig = concat!(
        "STRING=\"string2\"\n",
        "INT=2\n",
        "HEX=0x2\n",
        "BOOL=n\n",
        "TRISTATE=y\n"
    )
    .to_owned();

    let mut registry = registry_from(&input)?;
    registry.loadbuf(&dotconfig.into_bytes())?;

    let string_ref = ItemReference::Config("STRING".to_owned());
    let int_ref = ItemReference::Config("INT".to_owned());
    let hex_ref = ItemReference::Config("HEX".to_owned());
    let bool_ref = ItemReference::Config("BOOL".to_owned());
    let tristate_ref = ItemReference::Config("TRISTATE".to_owned());

    assert_eq!(
        Variant::from("string2"),
        registry.current_value(&string_ref).unwrap()
    );

    assert_eq!(Variant::from(2), registry.current_value(&int_ref).unwrap());

    assert_eq!(
        Variant::from(Hex::from(0x2)),
        registry.current_value(&hex_ref).unwrap()
    );

    assert_eq!(
        Variant::from(false),
        registry.current_value(&bool_ref).unwrap()
    );

    assert_eq!(
        Variant::from(Tristate::TRUE),
        registry.current_value(&tristate_ref).unwrap()
    );

    assert_eq!(false, registry.current_value_is_default(&string_ref));
    assert_eq!(false, registry.current_value_is_default(&int_ref));
    assert_eq!(false, registry.current_value_is_default(&hex_ref));
    assert_eq!(false, registry.current_value_is_default(&bool_ref));
    assert_eq!(false, registry.current_value_is_default(&tristate_ref));
    Ok(())
}

#[test]
fn test_dependency_on_macro() -> Result<(), Error> {
    let input = concat!(
        "ENABLED := y\n",
        "DISABLED := n\n",
        "if ENABLED\n config ENABLED_ITEM\n                   string   \"enabled_item\"\n endif\n",
        "if DISABLED\n config DISABLED_ITEM\n                 string   \"disabled_item\"\n endif\n",
        "if ENABLED && DISABLED\n config REALLY_DISABLED_ITEM string   \"really_disabled_item\"\n endif\n",
        "if ENABLED || DISABLED\n config REALLY_ENABLED_ITEM  string   \"really_enabled_item\"\n endif\n",
    );
    let registry = registry_from(&input)?;
    let enabled_ref = ItemReference::Config("ENABLED_ITEM".to_owned());
    let disabled_ref = ItemReference::Config("DISABLED_ITEM".to_owned());
    let really_disabled_ref = ItemReference::Config("REALLY_DISABLED_ITEM".to_owned());
    let really_enabled_ref = ItemReference::Config("REALLY_ENABLED_ITEM".to_owned());
    assert_eq!(true, registry.item_enabled(&enabled_ref)?);
    assert_eq!(false, registry.item_enabled(&disabled_ref)?);
    assert_eq!(false, registry.item_enabled(&really_disabled_ref)?);
    assert_eq!(true, registry.item_enabled(&really_enabled_ref)?);
    Ok(())
}

#[test]
fn test_dependency_direct_expressions() -> Result<(), Error> {
    let input = concat!(
        "if 1 < 2\n  config LT_ITEM\n               string   \"lt_item\"\n  endif\n",
        "if 1 > 2\n  config GT_ITEM\n               string   \"gt_item\"\n  endif\n",
        "if 1 <= 2\n config LTE_ITEM\n              string   \"lt_item\"\n  endif\n",
        "if 1 >= 2\n config GTE_ITEM\n              string   \"gt_item\"\n  endif\n",
        "if 1 == 1\n config EQ_ITEM\n               string   \"eq_item\"\n  endif\n",
        "if 1 != 1\n config NE_ITEM\n               string   \"ne_item\"\n  endif\n",
        "if !y\n     config NOT_ITEM\n              string   \"not_item\"\n endif\n",
        "if (y)\n    config SUB_ITEM\n              string   \"sub_item\"\n endif\n",
        "if y && n\n config AND_ITEM\n              string   \"gt_item\"\n  endif\n",
        "if y || n\n config OR_ITEM\n               string   \"lt_item\"\n  endif\n",
    );
    let registry = registry_from(&input)?;
    let lt_ref = ItemReference::Config("LT_ITEM".to_owned());
    let gt_ref = ItemReference::Config("GT_ITEM".to_owned());
    let eq_ref = ItemReference::Config("EQ_ITEM".to_owned());
    let ne_ref = ItemReference::Config("NE_ITEM".to_owned());
    let not_ref = ItemReference::Config("NOT_ITEM".to_owned());
    let sub_ref = ItemReference::Config("SUB_ITEM".to_owned());
    let and_ref = ItemReference::Config("AND_ITEM".to_owned());
    let or_ref = ItemReference::Config("OR_ITEM".to_owned());
    assert_eq!(true, registry.item_enabled(&lt_ref)?);
    assert_eq!(false, registry.item_enabled(&gt_ref)?);
    assert_eq!(true, registry.item_enabled(&eq_ref)?);
    assert_eq!(false, registry.item_enabled(&ne_ref)?);
    assert_eq!(false, registry.item_enabled(&not_ref)?);
    assert_eq!(true, registry.item_enabled(&sub_ref)?);
    assert_eq!(false, registry.item_enabled(&and_ref)?);
    assert_eq!(true, registry.item_enabled(&or_ref)?);
    Ok(())
}

#[test]
fn test_depends_on() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY\n bool \"dependency\"\n default y\n",
        "config DEPENDANT\n  bool \"dependant\"\n  depends on DEPENDENCY\n",
    );
    let mut registry = registry_from(&input)?;
    let dependency_ref = ItemReference::Config("DEPENDENCY".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(true, registry.item_enabled(&dependency_ref)?);
    assert_eq!(true, registry.item_enabled(&dependant_ref)?);

    registry.set_value(&dependency_ref, Variant::from(false))?;

    assert_eq!(true, registry.item_enabled(&dependency_ref)?);
    assert_eq!(false, registry.item_enabled(&dependant_ref)?);

    Ok(())
}

#[test]
fn test_depends_on_multiple() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY1\n bool \"dependency1\"\n default y\n",
        "config DEPENDENCY2\n bool \"dependency2\"\n default n\n",
        "config DEPENDANT\n  bool \"dependant\"\n  depends on DEPENDENCY1 && DEPENDENCY2\n",
    );
    let mut registry = registry_from(&input)?;
    let dependency2_ref = ItemReference::Config("DEPENDENCY2".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(false, registry.item_enabled(&dependant_ref)?);
    registry.set_value(&dependency2_ref, Variant::from(true))?;
    assert_eq!(true, registry.item_enabled(&dependant_ref)?);

    Ok(())
}

#[test]
fn test_default_depends() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY\n bool \"dependency\"\n default y\n",
        "config DEPENDANT\n  bool \"dependant\"\n  default y if DEPENDENCY\n default n if !DEPENDENCY",
    );
    let mut registry = registry_from(&input)?;
    let dependency_ref = ItemReference::Config("DEPENDENCY".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(
        Variant::from(true),
        registry.current_value(&dependant_ref).unwrap().into()
    );
    registry.set_value(&dependency_ref, Variant::from(false))?;
    assert_eq!(
        Variant::from(false),
        registry.current_value(&dependant_ref).unwrap().into()
    );

    Ok(())
}

#[test]
fn test_type_depends() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY\n bool \"dependency\"\n default y\n",
        "config DEPENDANT\n  bool \"dependant\" if DEPENDENCY\n string \"dependant\" if !DEPENDENCY\n",
    );
    let mut registry = registry_from(&input)?;
    let dependency_ref = ItemReference::Config("DEPENDENCY".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(
        VariantDataType::Bool,
        registry.current_value(&dependant_ref).unwrap().datatype()
    );
    registry.set_value(&dependency_ref, Variant::from(false))?;
    assert_eq!(
        VariantDataType::String,
        registry.current_value(&dependant_ref).unwrap().datatype()
    );

    Ok(())
}

#[test]
fn test_selects() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY\n select DEPENDANT\n tristate \"dependency\"\n default m\n",
        "config DEPENDANT\n  tristate \"dependant\"",
    );
    let mut registry = registry_from(&input)?;
    let dependency_ref = ItemReference::Config("DEPENDENCY".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency_ref, Variant::from(Tristate::FALSE))?;
    assert_eq!(
        Variant::from(Tristate::FALSE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency_ref, Variant::from(Tristate::TRUE))?;
    assert_eq!(
        Variant::from(Tristate::TRUE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency_ref, Variant::from(Tristate::MAYBE))?;
    registry.set_value(&dependant_ref, Variant::from(Tristate::TRUE))?;
    assert_eq!(
        Variant::from(Tristate::TRUE),
        registry.current_value(&dependant_ref).unwrap()
    );

    Ok(())
}

#[test]
fn test_implies() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY\n imply DEPENDANT\n tristate \"dependency\"\n default m\n",
        "config DEPENDANT\n  tristate \"dependant\"",
    );
    let mut registry = registry_from(&input)?;
    let dependency_ref = ItemReference::Config("DEPENDENCY".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency_ref, Variant::from(Tristate::FALSE))?;
    assert_eq!(
        Variant::from(Tristate::FALSE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency_ref, Variant::from(Tristate::TRUE))?;
    assert_eq!(
        Variant::from(Tristate::TRUE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency_ref, Variant::from(Tristate::MAYBE))?;
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependant_ref, Variant::from(Tristate::TRUE))?;
    assert_eq!(
        Variant::from(Tristate::TRUE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.discard_value(&dependant_ref)?;
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );

    registry.set_value(&dependency_ref, Variant::from(Tristate::FALSE))?;
    assert_eq!(
        Variant::from(Tristate::FALSE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependant_ref, Variant::from(Tristate::MAYBE))?;
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );

    Ok(())
}

#[test]
fn test_selects_multi() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY\n select DEPENDANT1\n select DEPENDANT2\n tristate \"dependency\"\n default m\n",
        "config DEPENDANT1\n  tristate \"dependant1\"\n",
        "config DEPENDANT2\n  tristate \"dependant2\"\n",
    );
    let registry = registry_from(&input)?;
    let dependant1_ref = ItemReference::Config("DEPENDANT1".to_owned());
    let dependant2_ref = ItemReference::Config("DEPENDANT1".to_owned());
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant1_ref).unwrap()
    );
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant2_ref).unwrap()
    );

    Ok(())
}

#[test]
fn test_implies_multi() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY\n imply DEPENDANT1\n imply DEPENDANT2\n tristate \"dependency\"\n default m\n",
        "config DEPENDANT1\n  tristate \"dependant1\"\n",
        "config DEPENDANT2\n  tristate \"dependant2\"\n",
    );
    let registry = registry_from(&input)?;
    let dependant1_ref = ItemReference::Config("DEPENDANT1".to_owned());
    let dependant2_ref = ItemReference::Config("DEPENDANT1".to_owned());
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant1_ref).unwrap()
    );
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant2_ref).unwrap()
    );

    Ok(())
}

#[test]
fn test_selected_by_multi() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY1\n select DEPENDANT\n tristate \"dependency\"\n default m\n",
        "config DEPENDENCY2\n select DEPENDANT\n tristate \"dependency\"\n default m\n",
        "config DEPENDANT\n  tristate \"dependant\"\n",
    );
    let mut registry = registry_from(&input)?;
    let dependency1_ref = ItemReference::Config("DEPENDENCY1".to_owned());
    let dependency2_ref = ItemReference::Config("DEPENDENCY2".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency1_ref, Variant::from(Tristate::FALSE))?;
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency2_ref, Variant::from(Tristate::FALSE))?;
    assert_eq!(
        Variant::from(Tristate::FALSE),
        registry.current_value(&dependant_ref).unwrap()
    );

    Ok(())
}

#[test]
fn test_implied_by_multi() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY1\n imply DEPENDANT\n tristate \"dependency\"\n default m\n",
        "config DEPENDENCY2\n imply DEPENDANT\n tristate \"dependency\"\n default m\n",
        "config DEPENDANT\n  tristate \"dependant\"\n",
    );
    let mut registry = registry_from(&input)?;
    let dependency1_ref = ItemReference::Config("DEPENDENCY1".to_owned());
    let dependency2_ref = ItemReference::Config("DEPENDENCY2".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency1_ref, Variant::from(Tristate::FALSE))?;
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency2_ref, Variant::from(Tristate::FALSE))?;
    assert_eq!(
        Variant::from(Tristate::FALSE),
        registry.current_value(&dependant_ref).unwrap()
    );

    Ok(())
}

#[test]
fn test_select_with_condition() -> Result<(), Error> {
    let input = concat!(
        "config PARENT\n bool \"parent\"\n default y\n",
        "config DEPENDENCY\n select DEPENDANT if PARENT\n tristate \"dependency\"\n default m\n",
        "config DEPENDANT\n  tristate \"dependant\"\n",
    );
    let mut registry = registry_from(&input)?;
    let parent_ref = ItemReference::Config("PARENT".to_owned());
    let dependency_ref = ItemReference::Config("DEPENDENCY".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&parent_ref, Variant::from(Tristate::FALSE))?;
    assert_eq!(
        Variant::from(Tristate::FALSE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&parent_ref, Variant::from(Tristate::TRUE))?;
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency_ref, Variant::from(Tristate::TRUE))?;
    assert_eq!(
        Variant::from(Tristate::TRUE),
        registry.current_value(&dependant_ref).unwrap()
    );

    Ok(())
}

#[test]
fn test_imply_with_condition() -> Result<(), Error> {
    let input = concat!(
        "config PARENT\n bool \"parent\"\n default y\n",
        "config DEPENDENCY\n imply DEPENDANT if PARENT\n tristate \"dependency\"\n default m\n",
        "config DEPENDANT\n  tristate \"dependant\"\n",
    );
    let mut registry = registry_from(&input)?;
    let parent_ref = ItemReference::Config("PARENT".to_owned());
    let dependency_ref = ItemReference::Config("DEPENDENCY".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&parent_ref, Variant::from(Tristate::FALSE))?;
    assert_eq!(
        Variant::from(Tristate::FALSE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&parent_ref, Variant::from(Tristate::TRUE))?;
    assert_eq!(
        Variant::from(Tristate::MAYBE),
        registry.current_value(&dependant_ref).unwrap()
    );
    registry.set_value(&dependency_ref, Variant::from(Tristate::TRUE))?;
    assert_eq!(
        Variant::from(Tristate::TRUE),
        registry.current_value(&dependant_ref).unwrap()
    );

    Ok(())
}

#[test]
fn test_switch_config_type() -> Result<(), Error> {
    let input = concat!(
        "config DEPENDENCY\n int \"dependency\"\n",
        "config DEPENDANT\n",
        "string \"foo\" if DEPENDENCY == 0\n",
        "int \"foo\" if DEPENDENCY == 1\n",
        "hex \"foo\" if DEPENDENCY == 2\n",
        "bool \"foo\" if DEPENDENCY == 3\n",
        "tristate \"foo\" if DEPENDENCY == 4\n",
    );

    let mut registry = registry_from(&input)?;
    let dependency_ref = ItemReference::Config("DEPENDENCY".to_owned());
    let dependant_ref = ItemReference::Config("DEPENDANT".to_owned());
    registry.set_value(&dependency_ref, Variant::from(0))?;
    assert_eq!(
        VariantDataType::String,
        registry.current_value(&dependant_ref).unwrap().datatype()
    );
    registry.set_value(&dependency_ref, Variant::from(1))?;
    assert_eq!(
        VariantDataType::Int,
        registry.current_value(&dependant_ref).unwrap().datatype()
    );
    registry.set_value(&dependency_ref, Variant::from(2))?;
    assert_eq!(
        VariantDataType::Hex,
        registry.current_value(&dependant_ref).unwrap().datatype()
    );
    registry.set_value(&dependency_ref, Variant::from(3))?;
    assert_eq!(
        VariantDataType::Bool,
        registry.current_value(&dependant_ref).unwrap().datatype()
    );
    registry.set_value(&dependency_ref, Variant::from(4))?;
    assert_eq!(
        VariantDataType::Tristate,
        registry.current_value(&dependant_ref).unwrap().datatype()
    );

    Ok(())
}

#[test]
fn test_menu_config() -> Result<(), Error> {
    let mut registry = registry_from(&"menuconfig FOO\n    string \"foo\"")?;
    let itemref = ItemReference::MenuConfig("FOO".to_owned());
    registry.set_value(&itemref, Variant::from("bar"))?;
    let out = String::from_utf8(registry.writebuf()?).unwrap();
    assert_eq!(true, out.contains("FOO=\"bar\""));
    let item = registry.get(&itemref).unwrap();
    assert_eq!(1, item.config_items().len());
    for (_, config_item) in item.config_items() {
        assert_eq!(true, config_item.is_menu());
    }
    Ok(())
}
