# Kconfig represent - for Cargo Kconfig

This module enables a representation in Rust types of a Kconfig file, using the
kconfig-parser module to parse the Kconfig file. Further, this module allows to
write and load the .config files.

It's main goal is to be the backend for cargo-kconfig, allowing kconfig to be
used through cargo, but it can also be used to create other frontends.
